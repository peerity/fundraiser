module.exports = {
  networks: {
    development: {
      host: "localhost",
      port: 8545,
      network_id: "*", // Match any network id
      gas: 2164567
    },
    rinkeby: {
      host: "localhost", // Connect to geth on the specified
      port: 8545,
      from: "0x4c121aAA2689C18c299773ef2de5c156B7427c33", // default address to use for any transaction Truffle makes during migrations
      network_id: 4, //rinkeby
      gas: 2164567 // Gas limit for deploys. Too high and you will get Error: exceeds block gas limit. Too low and you will get: Error: intrinsic gas too low
    },
    kovan: {
      host: "localhost", // Connect to geth on the specified
      port: 8545,
      from: "0x00A7ae6E06412643347C0aAfFB78b2CAD4682056", // default address to use for any transaction Truffle makes during migrations
      network_id: 42, //kovan
      gas: 2164567 // Gas limit for deploys. Too high and you will get Error: exceeds block gas limit. Too low and you will get: Error: intrinsic gas too low
    },
    live: {
      host: "localhost", // Connect to geth on the specified
      port: 8545,
      network_id: 1, //main
      gas: 2164567 //
    }
  },
  mocha: {
      useColors: true
  }
};
// https://github.com/trufflesuite/truffle/issues/522  - *any* error deploying contract with Truffle gives 'The contract code couldn't be stored, please check your gas amount.'
// this part about the gas is just a guess from the person that wrote the error message. It could be other issues with the contract code.
// One reason this can can happen if the constructor parameters aren't provided correctly in the deploy_contracts.js