pragma solidity 0.4.15;

import "./StandardToken.sol";

contract PeerityToken is StandardToken {

    string public constant name = "Peerity Interim Token";
    string public constant symbol = "PEER";
    uint8 public constant decimals = 18;
    string public constant version = "0.1";

    // Aproximate 24 hours in block time
    uint32 public constant BLOCK_DAY = 3504;

    // Deployed contract owner, set in constructor.  
    address public contractOwnerAddress;

    // Escrow wallet address must be explicily set before fundraising starts.
    // On deployment, defaults to the contractOwnerAddress
    address public escrowAddress;

    // Tracks the fundingPhase of the fundraiser. 
    enum FundingPhase { One, Two, Finalized, Redeeming } 
    enum ContractState { Running, Paused, Emergency } 
 
    FundingPhase public fundingPhase;           // Current fundingPhase of the contract 
    ContractState public contractState; 

    //Funding start is the beginning of the first phase of the fundraiser.
    uint256 public fundingStartBlock;
    uint256 public fundingPauseBlock;        // -- end of first phase of fundraising.
    uint256 public fundingEndBlock;          // -- the FINAL end of the fundraiser.

    uint256 public tierOneExchangeRate;
    uint256 public tierTwoExchangeRate;
    uint256 public tierThreeExchangeRate;

    //only allow finalized to be called once. End of successful fundraiser and beginning of redeming period 
    bool private hasBeenFinalized = false;

    //Rates are currently set only once, before fundraising begins. When set, prevents further updates
    bool private rateIsSet = false;

    //Various token caps that decide the total tokens minted.
    //Caps that are not reached will not contribute to the total tokens in circulation.
    uint256 public constant TOKEN_CREATION_CAP = 92.5e6 * 10**uint256(decimals); // 80 million Peerity for public fundraiser + 12.5 for presale. 7.5mil are for the team and will be on genesis block of live network.
    uint256 public constant PHASE_ONE_CREATION_CAP = 40e6 * 10**uint256(decimals); //40 million Peerity.  20 for the sale and 20 for presale grants/founders.
    uint256 public constant PHASE_TWO_CREATION_CAP = 70e6 * 10**uint256(decimals); //70 million Peerity.  50 for the sale and 20 for presale grants/founders.
    uint256 public constant PHASE_ONE_CREATION_FLEX = 42e6 * 10**uint256(decimals); //allow a small overshoot in phase one.

    //Limit per person for phase 1.
    uint256 public constant TOKEN_MIN = 1 * 10**uint256(decimals); // 1 Peerity

    uint256 public constant PHASE_ONE_BUY_CAP = 700 ether; // Minium donations accepted.
    uint256 public constant ETH_RECEIVED_CAP = 125000 ether; // ~40m USD depending on exchange rates
    uint256 public constant ETH_RECEIVED_MIN = 1000 ether ; // 1 000 ETH
    uint256 public constant ETH_SOFT_CAP =  68000 ether;//if we cross this, funding end block accelerates

    // We need to keep track of how much ether have been contributed, since we have a cap for ETH too
    uint256 public totalReceivedEth = 0;

    // Since we have different exchange rates at different stages, we need to keep track
    // of how much ether each contributed in case that we need to issue a refund
    mapping (address => uint256) private ethBalances;

    // Events used for logging
    event EmergencyStop();
    event LogRefund(address indexed _to, uint256 _value);
    event LogCreatePEER(address indexed _to, uint256 _value);
    event LogReedemPresale(address indexed _to, uint256 _value);
    event LogRedeemPEER(address indexed _to, uint256 _value, bytes32 _holderAddress);
    event LogStartRedeeming();
    event FundraisingCapTriggered();
    event FundraisingPaused();
    event FundraisingPhaseOneOver();
    event FundraisingResumed();
    event FundraiserOver();
    event EscrowAddressSet(address _escrowAddress);
 
    modifier onlyPhase(FundingPhase _phase) { 
        require(fundingPhase == _phase); 
        _;
    }
 
    modifier onlyIfFundraiserSuccessful() {
        require(hasBeenFinalized==true || (block.number > fundingEndBlock && totalReceivedEth >= ETH_RECEIVED_MIN));
        _;
    }

    modifier onlyWhenFundraising() {
        require(block.number >= fundingStartBlock && block.number <= fundingEndBlock);
        require((fundingPhase == FundingPhase.One) || (fundingPhase == FundingPhase.Two));
        require(contractState == ContractState.Running);
        require(tierOneExchangeRate > 0 && tierTwoExchangeRate > 0 && tierThreeExchangeRate > 0);
        _;
    }

    modifier onlyBeforeFundraisingStart() {
        require(block.number < fundingStartBlock);
        _;
    }

    modifier onlyAfterFundraisingEnd() {
        require(block.number >= fundingEndBlock);
        _;
    }

    modifier onlyWhenNoEmergency() {
        require(contractState != ContractState.Emergency);
        _;
    }

    modifier onlyOwner() {
        require(msg.sender == contractOwnerAddress);
        _;
    }

    modifier beforeFinalized() {
        require(hasBeenFinalized == false);
        _;
    }

    modifier onlyWhenMinimumReached() {
        require(totalReceivedEth >= ETH_RECEIVED_MIN);
        _;
    }

    modifier onlyWhenMinimumNotReached() {
        require(totalReceivedEth < ETH_RECEIVED_MIN);  // No refunds if the minimum has been reached
        _;
    }

    // Constructor
    function PeerityToken(
        address _contractOwnerAddress,
        uint256 _fundingStartBlock,
        uint256 _fundingPauseBlock,
        uint256 _fundingEndBlock) {

        require(block.number < _fundingStartBlock); // The start of the fundraising should happen in the future
        require(_fundingStartBlock < _fundingPauseBlock);
        require(_fundingPauseBlock < _fundingEndBlock);

        fundingPhase = FundingPhase.One; 
        contractState = ContractState.Running;

        contractOwnerAddress = _contractOwnerAddress;
        escrowAddress = _contractOwnerAddress; //by default the same, will be updated with escrow partner as owner if we use escrow.
        fundingStartBlock = _fundingStartBlock;
        fundingPauseBlock = _fundingPauseBlock;
        fundingEndBlock = _fundingEndBlock;
        totalSupply = 0;
    }

    // Explicitly set the multi-sig wallet address to something other than owner
    function setEscrowAddress(address _escrowAddress)
    external
    onlyOwner
    onlyBeforeFundraisingStart
    {
        escrowAddress = _escrowAddress;
        EscrowAddressSet(_escrowAddress);
    }

    function setTierExchangeRate(uint256 _tierOneExchangeRate, uint256 _tierTwoExchangeRate, uint256 _tierThreeExchangeRate)
    external
    onlyOwner
    onlyBeforeFundraisingStart
    {
        tierOneExchangeRate = _tierOneExchangeRate;
        tierTwoExchangeRate = _tierTwoExchangeRate;
        tierThreeExchangeRate = _tierThreeExchangeRate;
        rateIsSet = true;
    }

    // ERC20 Overridden method to check for end of fundraising before allowing transfer of tokens
    function transfer(address _to, uint256 _value)
    onlyIfFundraiserSuccessful
    returns (bool success)
    {
        return super.transfer(_to, _value);
    }

    // ERC20 Overridden method to check for end of fundraising before allowing transfer of tokens
    function transferFrom(address _from, address _to, uint256 _value)
    onlyIfFundraiserSuccessful
    returns (bool success)
    {
        return super.transferFrom(_from, _to, _value);
    }

    // Used to grant tokens to pre-sale buyers.  Still counts against token creation limit.
    function grantPresaleTokens(address _recipient, uint256 _tokens)
    onlyOwner
    beforeFinalized
    external
    {
        //We need to grant token after the start of fundraising in case we have to chase people down for their ETH address.
        //They need to get back to us before finalization.
        uint256 newTotalSupply = safeAdd(totalSupply, _tokens);
        require(newTotalSupply<=TOKEN_CREATION_CAP);
        totalSupply = newTotalSupply;
        balances[_recipient] = safeAdd(balances[_recipient],_tokens);
        LogCreatePEER(_recipient, _tokens);
        //Log so presale people can verify that a redemption is theirs and we can programmatically verify we got everyone.
        LogReedemPresale(_recipient, _tokens); 
    }

    /**
    * Used to end Phase1.
    * After Phase1 end, if minimum is reached owner can use retrieveEth() to move Eth to fund wallet
    */
    function endPhaseOne()
    external
    onlyPhase(FundingPhase.One)
    onlyOwner
    {
        fundingPhase = FundingPhase.Two;
        contractState = ContractState.Paused;
        FundraisingPhaseOneOver();
    }

    /// @dev Accepts ether and creates new ERC20 PEER tokens
    // This is the meat of the sale contract.
    function createTokens()
    payable
    external
    onlyWhenFundraising
    {
        require(msg.value > 0);

        // reject the contribution if the max cap has been reached.
        uint256 newReceivedTotal = safeAdd(totalReceivedEth, msg.value);
        require(newReceivedTotal <= ETH_RECEIVED_CAP);

        // require minimum amount of tokens minted when based on current tier
        uint256 tokens = safeMult(msg.value, this.getCurrentTokenPrice());
        require(tokens >= TOKEN_MIN);

        uint256 newTotalSupply = safeAdd(totalSupply, tokens);
        require(newTotalSupply <= TOKEN_CREATION_CAP);

        //Check token caps in Phase one.
        if (fundingPhase == FundingPhase.One) {
            require(block.number < fundingPauseBlock);
            require(safeAdd(ethBalances[msg.sender], msg.value) <= PHASE_ONE_BUY_CAP); //make sure that they don't buy over the cap.
            require(newTotalSupply <= PHASE_ONE_CREATION_FLEX);
        }

        // When all the checks have passed, update the balances of the contract
        ethBalances[msg.sender] = safeAdd(ethBalances[msg.sender], msg.value);
        totalReceivedEth = newReceivedTotal;
        totalSupply = newTotalSupply;
        balances[msg.sender] = safeAdd(balances[msg.sender], tokens);

        LogCreatePEER(msg.sender, tokens);

        //If we hit soft cap, accelerate the fundingEndBlock to end in aproximatly 48 hrs (2 Block Days).
        if (totalReceivedEth >= ETH_SOFT_CAP) {
            uint256 newEndBlock = block.number + (BLOCK_DAY*2);
            if (newEndBlock < fundingEndBlock) {
                fundingEndBlock = newEndBlock;
            }
            FundraisingCapTriggered();
        }

        //End phase 1 automatically if we exceed the phase1 cap.
        //This cannot be done by simply calling endPhaseOne as that manual process is owner restricted.
        //This will automatically pause fundraising, Owner must officially declare phase1 done.
        if ((fundingPhase == FundingPhase.One) && (totalSupply>=PHASE_ONE_CREATION_CAP)) {
            fundingPhase = FundingPhase.Two;
            contractState = ContractState.Paused;
            FundraisingPaused();
            FundraisingPhaseOneOver();
        }
    }

    /// @dev Returns the current token price. External in case automated investment listeners are interested.
    function getCurrentTokenPrice()
    external
    constant
    returns (uint256 currentPrice)
    {
        if (block.number < fundingPauseBlock && totalSupply < PHASE_ONE_CREATION_CAP) {
            return tierOneExchangeRate;
        }
        if ( totalSupply < PHASE_TWO_CREATION_CAP) {
            return  tierTwoExchangeRate;
        }
        return tierThreeExchangeRate;
    }

    /// @dev Redeems PEERs and records by logging the Peerity wallet / address of the sender
    // The crowd fund buyer or PEER token holder can call this to generate a claim receipt
    // which can then be used by the Peerity team to preload the Peerity genesis block
    // with wallet addresses and ballances. This assumes whoever calls this method has
    // a Peerity wallet allready and can generate a valid Peerity chain address from a private key.
    // Once a recipt is logged on the ethereum blockchain a listener will
    // load the Peerity address with the ballances that the claimant deserves.
    function redeemTokens(bytes32 holderAddress)
    external
    onlyPhase(FundingPhase.Redeeming)
    {
        uint256 netVal = balances[msg.sender];
        require(netVal >= TOKEN_MIN); // At least TOKEN_MIN tokens have to be redeemed

        // Burns tokens and redeems them.
        balances[msg.sender] = 0;
        LogRedeemPEER(msg.sender, netVal, holderAddress);
    }

    /// @dev Allows to transfer ether from the contract as soon as the minimum is reached
    function retrieveEth()
    external
    onlyWhenMinimumReached
    onlyWhenNoEmergency
    onlyOwner
    {
        // send the eth to crowdfund Escrow/multisig wallet
        escrowAddress.transfer(this.balance);
    }

    // Ends the fundraising period and sends the ETH to the Multisig wallet
    // Can be called by anyone once fundraiser is over.
    function finalize()
    external
    beforeFinalized
    onlyAfterFundraisingEnd
    onlyWhenMinimumReached
    onlyWhenNoEmergency
    {
        hasBeenFinalized = true;
        fundingPhase = FundingPhase.Finalized;

        // Send the ETH to escrow of crowdfund Creators
        escrowAddress.transfer(this.balance);
        FundraiserOver();
    }

    // Starts the redeeming period, allowing users to migrate to our final solution.
    // The redeeming period can only be started after the contract is finalized
    // Our solution will listen for this event and redemption events and automatically credit users.
    function startRedeeming()
    external
    onlyPhase(FundingPhase.Finalized)
    onlyOwner
    {
        fundingPhase = FundingPhase.Redeeming;
        LogStartRedeeming();
    }

    // Pauses the fundraising by blocking contributions.  Message  is logged.
    // Manual pause by owner allowed anytime before finalize. Pause also happens automatically at end of phase 1
    function pause()
    external
    onlyOwner
    onlyWhenNoEmergency
    beforeFinalized
    {
        contractState = ContractState.Paused;
        FundraisingPaused();
    }

    //Can be used to resume fundraising if paused or emergency.
    function resumeFundraising()
    external
    onlyOwner
    beforeFinalized
    {
        contractState = ContractState.Running;
        FundraisingResumed();
    }

    /// @dev Allows contributors to recover their ether in case the minimum funding goal is not reached
    /// Refunding automatically available when min ether is not reached and end block is passed.
    // No Pause or Emergency can stop this, as a trust thing.  No matter what if we don't hit the goal, people can get refunds.
    function refund()
    external
    onlyWhenMinimumNotReached
    onlyAfterFundraisingEnd
    {
        uint256 netVal = balances[msg.sender];
        require(netVal > 0);
        uint256 ethVal = ethBalances[msg.sender];
        require(ethVal > 0);

        // Update the state only after all the checks have passed
        balances[msg.sender] = 0;
        ethBalances[msg.sender] = 0;
        totalSupply = safeSubtract(totalSupply, netVal); // Extra safe

        // Log this refund
        LogRefund(msg.sender, ethVal);

        // Send the contributions only after we have updated all the balances
        // If you're using a contract, make sure it works with .transfer() gas limits
        msg.sender.transfer(ethVal);
    }

    // Sets the contract state to 'Emergency'
    // This is different from pause which just blocks contributions.
    // Emergency Stop can be executed by owner at any time.
    function emergencyStop()
    external
    onlyOwner
    onlyWhenNoEmergency
    {
        contractState = ContractState.Emergency; // TODO: ****** untested ***************
        EmergencyStop();
    }

    // recover from disaster prior to end of fundraiser. Must be first put in Emergency state 
    function emergencyDrain()
    external
    onlyWhenMinimumReached
    onlyOwner
    {
        require(contractState==ContractState.Emergency);
        contractOwnerAddress.transfer(this.balance);
    }
}
