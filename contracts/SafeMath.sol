pragma solidity 0.4.15;

contract SafeMath {

    function safeAdd(uint256 x, uint256 y) internal constant returns(uint256 z) {
        require((z = x + y) >= x);
    }

    function safeSubtract(uint256 x, uint256 y) internal constant returns (uint256 z) {
        require((z = x - y) <= x);
    }

    function safeMult(uint x, uint y) internal returns (uint z) {
        require(y == 0 || (z = x * y) / y == x);
    }

    function safeDiv(uint256 a, uint256 b) internal constant returns (uint256 c) {
        c = a / b;
    }
}
