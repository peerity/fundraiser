# Peerity Fundraising and Token Exchange Contract
Fundraising Contract

## Run Testsuite
- run `npm install` first
- start TestRPC via `./startTestRPC.sh` - assumes linux or OSX

IMPORTANT: For each test suite, you must restart testRPC.  This will reset wallets for the different simulations.

## Multisig
The recipient address will be set to an instance of the Gnosis multi-sig wallet
https://github.com/gnosis/MultiSigWallet

# Peerity Crowdsale - English Spec

The Peerity crowdsale consists of two phases and 3 price points. These price points will be set before the beginning of the crowd sale or tokens will not be able to be sold.
The length of phase 1 and phase 2 will be chosen as part of contract creation and will therefore be public.

The sale will result in the existence of a max of 92.5million PEER.
Of the 100m initial tokens, 7.5million are reserved for the team.
However, these will not be available to the team until the launch of the live network, where they will be part of the genesis block.

* During fundraising, tokens can be purchased but not traded.

## Phase 1:
* There will be a maximum sale of 42million PEER.
* Of these PEER, ~20mil are reserved for pre-sale customers and the Peerity Team.
* Fundraising will bbe paused once the final block is reached or over 40m PEER are sold. However customers can buy up to 42m PEER as part of a soft cap. Once either the buy or time cap is reached, fundraising will pause.  Peerity will issue the official PhaseOneOver command and the event will be logged.
* Once any customer purchases over 40m PEER, the crowdsale will pause until Phase 2.
* There will be a max purchase of 700ETH per address during phase 1.  There is no cap during Phase 2.

The first crowdsale will be at a price of approximately $0.20 per PEER token.  This will be based on the current exchange rates by Peerity before the beginning of the sale.


## Phase 2:
Phase 2 will sell at a price point of ~$0.30 per PEER per token until 70million PEER are sold. Then the price per token will increase to ~$.40 per token.
* There will be a maximum sale of 92.5million PEER, including the 42million from Phase 1.  If Phase1 does not sell out, those tokens will still be available but at the new price point. Any tokens not sold will be not be created, lowering total supply.
* There will be a maximum raise of 125,000 ETH.
* There will be a soft-cap raise of 68,000 ETH. Once this soft-cap is hit, the crowd sale will automatically end in ~48 hours/7008 blocks. This timer is not meant to be exact on purpose.
* Once the block limit or cap is exceeded, no further ETH will be accepted.

## Fundraiser End
Once phase 2 is over, the fundraiser will either be Finalized or if the minimum raise is not reach, ETH will be available for refund.
* The minimum Raise is 1000 ETH.
* Once the minimum has been raised, the ETH can be withdrawn by Peerity at any time.
* Once the time limit or ETH limit is hit, the fundraiser will end and can be finalized.
* Once the fundraiser is declared finalized, all remaining funds will be sent to Peerity automatically
* Once finalized, token holders can trade them freely.

## Pausing the fundraiser
Peerity can pause the fundraiser at any time as an emergency brake in case of issues. In addition, fundraising will automatically pause at the end of phase1.
When pausing occurs, Peerity will log a message to the blockchain that this has happened along with a reason why, for total transparency.
Once the end block passes, the fundraiser will either be finalized or allow refunds.  A finalized sale means that no refunds will be offered.  
Whether or not Peerity officially calls the "finalize" function of the contract, a successful crowdsale will mean that tokens are available for trade immediately after both the Eth minimum has been passed and the end block has passed.
Similarly, if the end block passes and the ETH minimum raise has NOT been exceeded, refunds will be available without the intervention of Peerity.  

## Token Redemption
Once the Peerity network is finalized, token holders will be able to Redeem their tokens to a peerity address on the peerity blockchain.
This should log an event on the blockchain specifying their new address and hte number of tokens redeemed.  These tokens are then burned.
