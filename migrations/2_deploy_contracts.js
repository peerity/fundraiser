var PeerityToken = artifacts.require("./PeerityToken.sol");

module.exports = function(deployer, network) {
    if (network == "development") {
        var ethfund = "0x39aeeba1b26c59bb0239a7740a3aaf7aa3bc94aa";
        var start = 15;
        var pause = 40;
        var end = 80; //Needs to be set to a large number to test timeout trigger.
        deployer.deploy(PeerityToken, ethfund, start, pause, end, {gas: 2712388});
    }
    // for live net
    else if(network == "live") {
        var ethfund = "";
        var start = 0;
        var pause = 0;
        var end = 0; //Needs to be set to a large number to test timeout trigger.
        deployer.deploy(PeerityToken, ethfund, start, pause, end, {gas: 2712388});
    }

};
