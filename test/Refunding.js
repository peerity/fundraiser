var PeerityToken = artifacts.require("PeerityToken");
var assert = require('assert');
var BigNumber = require('bignumber.js');
BigNumber.config({ ERRORS: false });

async function mineBlocks(num=1) {
    for (let i=0; i<num; ++i) {
        await new Promise(function(resolve, reject) { web3.currentProvider.sendAsync({ jsonrpc: "2.0", method: "evm_mine", id: i }, function(err, result) { resolve(); }); });
    }
}

function lastBlock() {
    return new Promise(function(resolve, reject) {
        web3.currentProvider.sendAsync({ jsonrpc: "2.0", method: "eth_blockNumber", id: 0x05 }, function(err, result) { resolve(web3.eth.getBlock(parseInt(result.result))) });
    });
}

function blockNumber() {
    return new Promise(function(resolve, reject) {
        web3.currentProvider.sendAsync({ jsonrpc: "2.0", method: "eth_blockNumber", id: 0x05 }, function(err, result) { resolve(parseInt(result.result)) });
    });
}

function convertInt(val) {
    return val.toNumber();
}

function snapshot() {
    return new Promise(function(resolve, reject) {
        web3.currentProvider.sendAsync({ jsonrpc: "2.0", method: "evm_snapshot", id: 0xabcd }, function(err, result) { resolve(); });
    });
}

function revert() {
    return new Promise(function(resolve, reject) {
        web3.currentProvider.sendAsync({ jsonrpc: "2.0", method: "evm_revert", id: 0xabcd }, function(err, result) { resolve(); });
    });
}

function reset() {
    return new Promise(function(resolve, reject) {
        web3.currentProvider.sendAsync({ jsonrpc: "2.0", method: "evm_reset", id: 0xabce }, function(err, result) { resolve(); });
    });
}

function getEvent(event, result) {
    for (let i = 0; i < result.logs.length; i++) {
        const log = result.logs[i];

        if (log.event === event) {
            return log;
        }
    }
    return undefined;
}

contract('PeerityToken', function(accounts) {

    let fundingStartBlock = 0;
    let fundingPauseBlock = 0;
    let fundingEndBlock = 0;
    let tierOneExchangeRate = 0;
    let tierTwoExchangeRate = 0;
    let tierThreeExchangeRate = 0;
    let ethReceivedCap = 0;
    let blockDay= 0;
    let ethReceivedMin = 0;
    let tokenCreationCap = 0;
    let tokenMin = 0;
    let contractOwnerAddress = '';
    const initialFundBalance = 999580000200000000000; // there is already some gas deducted for the contract


    let tierOneTokenCap = 0;
    let tierOneTokenFlex = 0;
    let tierTwoTokenCap = 0;

    // match contract enum FundingPhase { One, Paused, Finalized, Redeeming } 
    const enumFundingPhaseOne = 0;
    const enumFundingPhaseTwo = 1;
    const enumFundingPhaseFinalized = 2;
    const enumFundingPhaseRedeeming = 3;

    // match contract enum ContractState { Paused, Emergency } 
    const enumContractStateRunning = 0;
    const enumContractStatePaused = 1;
    const enumContractStateEmergency = 2;

    // enum
    const isFundraising = 0;
    const isFinalized = 1;
    const isRedeeming = 2;
    const isPaused = 3;

    const weiToEther = 1000000000000000000;
    const standardBid =10000000000000000000; //10 ether
    const epicBid = 400000000000000000000;
    
    before(async function() {
        await reset();
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.fundingStartBlock.call();
        }).then(result => {
            fundingStartBlock = convertInt(result);
            return pt.fundingEndBlock.call();
        }).then(result=>{
            fundingEndBlock = convertInt(result);
            return pt.fundingPauseBlock.call();
        }).then(result => {
            fundingPauseBlock = convertInt(result);
            return pt.ETH_RECEIVED_CAP.call();
        }).then(result => {
            ethReceivedCap = convertInt(result);
            return pt.ETH_RECEIVED_MIN.call();
        }).then(result => {
            ethReceivedMin = convertInt(result);
            return pt.TOKEN_CREATION_CAP.call();
        }).then(result => {
            tokenCreationCap = convertInt(result);
            return pt.TOKEN_MIN.call();
        }).then(result => {
            tokenMin = convertInt(result);
            return pt.contractOwnerAddress.call();
        }).then(result => {
            contractOwnerAddress = result;
            assert(true, "contract owner is "+contractOwnerAddress);
            return pt.BLOCK_DAY.call();
        }).then(result => {
            blockDay = convertInt(result);
            return pt.PHASE_TWO_CREATION_CAP.call();
        }).then(result => {
            tierTwoTokenCap = convertInt(result);
            return pt.PHASE_ONE_CREATION_CAP.call();
        }).then(result=>{
            tierOneTokenCap = convertInt(result);
            return pt.PHASE_ONE_CREATION_FLEX.call();
        }).then(result=>{
            tierOneTokenFlex=convertInt(result);
        });
    });


    /**
     *
     *
     *  PHASE 1 SALE TESTS
     *
     *
     */

    it('should have exactly 92,500,000 tokens, with 18 decimal places',function(){
        return PeerityToken.deployed().then(instance => {
            return instance.TOKEN_CREATION_CAP.call()
        }).then((supply) => {
            let supplyInt = convertInt(supply);
            expectedSupply = new BigNumber(92500000000000000000000000);
            assert.ok(expectedSupply.equals(supplyInt));
            let max = 925 * (10**5) * 10**18;
            assert.ok(expectedSupply.equals(max));
        })
    });

    it('should start at block 4', function() {
        return PeerityToken.deployed().then(instance => {
            return blockNumber();
        }).then(block => {
            assert.equal(block, 4, 'after deploying we should be at block 4, perhaps you should restart testrpc');
        });
    });

    it('Contract should have the initialFundBalance minus used gas', function() {
        const gasUsed = 2099999;
        const gasPrice = 20000000000;

        return PeerityToken.deployed()
            .then(() => {
                return web3.eth.getBalance(contractOwnerAddress);
            }).then(balance => {
                assert.ok(balance >= initialFundBalance - (gasUsed * gasPrice), 'ethFundDeposit wrong initial balance');
            }).catch(() => {
                assert.fail('could not get balance of ethFundDeposit');
            });
    });


    it('should have a fundingStartBlock in the future', function() {
        return blockNumber().then(currentBlock => {
            assert.ok(currentBlock < (fundingStartBlock - 1), 'fundingStartBlock is not in the future');
        });
    });

    it('is not yet finalized/redeeming', function() {
        return PeerityToken.deployed().then(pt => {
            return pt.fundingPhase.call();
        }).then(fundingPhase => {
            assert.ok(convertInt(fundingPhase) === enumFundingPhaseOne, 'should not be finalized');
        })
    });

    it('no finalization before the beginning', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.finalize({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('should not be possible to finalize');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('should not be possible to finalize');
            }
        });
    });

    it('no redeeming period before the beginning', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.startRedeeming({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('should not be possible to start redeeming phase');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('should not be possible to start redeeming phase');
            }
        });
    });

    it('should not issue tokens before the beginning', function() {
        return PeerityToken.deployed().then(pt => {
            return pt.createTokens({
                from: accounts[0],
                value: standardBid,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('token creation did not fail');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('token creation did not fail');
            }
        });
    });

    it('should allow setting exchange tiers during presale', function() {
        //these should be tokens per eth.  Because we have the same number of decimals, it works with wei.  Keep decimals the same.
        let e1 = 1500;
        let e2 = 1000;
        let e3 = 750;
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return blockNumber();
        }).then(() => {
            return pt.setTierExchangeRate(e1, e2, e3, {
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.tierOneExchangeRate.call();
        }).then(tierOneEx => {
            tierOneExchangeRate = tierOneEx;
            assert.equal(e1, tierOneExchangeRate, 'setting exchange tiers failed: expected ' + e1 + ' but was ' + tierOneExchangeRate );
            return pt.tierTwoExchangeRate.call();
        }).then(tierTwoEx => {
            tierTwoExchangeRate = convertInt(tierTwoEx);
            assert.equal(e2, tierTwoExchangeRate, 'setting exchange tiers failed: expected ' + e2 + ' but was ' + tierTwoEx );
            return pt.tierThreeExchangeRate.call();
        }).then(tierThreeEx => {
            tierThreeExchangeRate = convertInt(tierThreeEx);
            assert.equal(e3, tierThreeExchangeRate, 'setting exchange tiers failed: expected ' + e3 + ' but was ' + tierThreeExchangeRate );

        }).catch(() => {
            assert.fail('setting exchange tiers during presale failed');
        });
    });



    it('take a snapshot of the blockchain', function() {
       return snapshot();
    });


    it('should allow giving tokens from presale', function() {
        let pt = null;
        const weis = standardBid;
        let tokenGrant = 20*(10**6) * weiToEther; //20 mil peerity, 18 decimals
        let blocknumber = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.grantPresaleTokens("0x3D7D9AF2BF88E91A9D73D10F79C278424DDC89D83D7D9AF2BF88E91A9D73D45A", tokenGrant, {
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.balanceOf.call("0x3D7D9AF2BF88E91A9D73D10F79C278424DDC89D83D7D9AF2BF88E91A9D73D45A");
        }).then(balance => {
            assert.equal(balance, tokenGrant, 'got wrong amount of tokens');
        }).catch(() => {
            assert.fail('Granting tokens failed');
        });
    });


    it('should not allow giving tokens without being owner', function() {
        let pt = null;
        const weis = standardBid;
        let tokenGrant = 10000;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return blockNumber();
        }).then(() => {
            return pt.grantPresaleTokens("0x3D7D9AF2BF88E91A9D73D10F79C278424DDC89D83D7D9AF2BF88E91A9D73D45A", tokenGrant, {
                from: "0x3D7D9AF2BF88E91A9D73D10F79C278424DDC89D8",
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.balanceOf.call("0x3D7D9AF2BF88E91A9D73D10F79C278424DDC89D83D7D9AF2BF88E91A9D73D45A");
        }).then(balance => {
            assert.fail("Tokens granted by non-owner.")
        }).catch(() => {
            assert.ok(true);
        });
    });


    it('should issue tokens for the correct price in phase 1', function() {
        let pt = null;
        const weis = standardBid;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return blockNumber();
        }).then(currentBlock => {
            // mine blocks to get us to the first block of funding
            return mineBlocks(fundingStartBlock - currentBlock - 1);
        }).then(() => {
            return pt.createTokens({
                from: accounts[0],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.balanceOf.call(accounts[0]);
        }).then(balance => {
            assert.equal(balance, weis * tierOneExchangeRate, 'got wrong amount of tokens');
        }).catch(() => {
            assert.fail('token creation failed');
        });
    });

    it('should NOT allow changing rates after sale begins', function() {
        let e1 = 275;
        let e2 = 325;
        let e3 = 500;
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.setTierExchangeRate(e1, e2, e3, {
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).catch((e) => {
            if (e.name == 'Error') {
                assert.ok(true, "Can't change exchange rates after fundraising starts");
            } else {
                assert.fail('Could change exchange rates after launch');
            }
        })
    });

    it('should not allow people to buy beyond the phase1 individual limit', function() {
        let pt = null;
        const weis = 705*weiToEther;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.createTokens({
                from: accounts[21],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).catch(() => {
            assert.ok('token creation failed');
        });
    })

    it('should allow people to buy up to personal phase1 limit', function() {
        let pt = null;
        const weis = 699*weiToEther; //299 ETH, below the buy cap
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.createTokens({
                from: accounts[10],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret)=>{
            assert.ok(getEvent("LogCreatePEER", ret), "Bought peer");
        }).catch(e=>{
            assert.fail(e);
        })
    })


    it('should allow Peerity to end Phase 1 after the timeout.', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return blockNumber();
        }).then(currentBlock=>{
            return mineBlocks(fundingPauseBlock - currentBlock);
        }).then(()=>{
            return pt.endPhaseOne({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(()=>{
            return pt.fundingPhase.call();
        }).then((fundingPhase)=>{
            assert.ok(convertInt(fundingPhase) === enumFundingPhaseTwo, 'should be paused');
        });
    });

    it('should allow us to resume from Phase 1 to Phase 2', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.resumeFundraising({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            })
        }).then(()=>{
            return pt.contractState.call();
        }).then((contractState)=>{
            assert.ok(convertInt(contractState) === enumContractStateRunning, 'should be fundraising'); //TODO: assert equals
        });
    });

    /**
     *
     * END PHASE 1
     * TEST PHASE 2
     *
     */

    it('should issue tokens for the correct price in Phase 2', function() {
        let pt = null;
        const weis = standardBid;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.createTokens({
                from: accounts[1],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.balanceOf.call(accounts[1]);
        }).then(balance => {
            assert.equal(balance, weis * tierTwoExchangeRate, 'got wrong amount of tokens');
        }).catch(() => {
            assert.fail('token creation did fail');
        });
    });



    it('should not allow tokens to be traded during bidding in phase 2', function() {
        return PeerityToken.deployed().then(pt => {
            return pt.transfer(accounts[1], 10, {
                from: accounts[0],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('trading did not fail');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('token creation did not fail');
            }
        });
    });

    it('should be in phase two fundraising', function(){
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.fundingPhase.call();
        }).then((fundingPhase)=>{
            assert.ok(convertInt(fundingPhase) === enumFundingPhaseTwo, 'should be fundraising');
        });
    });


    it('should not issue tokens after the funding has ended (because of reaching fundingEndBlock)', function() {
        let pt = null;
        const weis = standardBid;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return blockNumber();
        }).then(currentBlock => {
            // mine necessary amount of blocks
            return mineBlocks(fundingEndBlock - currentBlock); // we need to be > than fundingEndBlock
        }).then(() => {
            return pt.createTokens({
                from: accounts[2],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(balance => {
            assert.fail('token creation did not fail');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('token creation did not fail');
            }
        });
    });


    it('only the owner can call pause', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.pause({
                from: accounts[0],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('pausing should not be allowed');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('pausing should not be allowed');
            }
        })
    });

    it('should not allow finalization', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            let contractBalance = web3.eth.getBalance(pt.address);
            assert.ok(contractBalance <= ethReceivedMin, "Raised under");
            return pt.fundingPhase.call();
        }).then((preFinalState)=>{
            assert.ok(convertInt(preFinalState) === enumFundingPhaseTwo);
            return pt.finalize({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.fundingPhase.call();
        }).then(fundingPhase => {
            let currentstate = convertInt(fundingPhase);
            assert.fail(currentstate === enumFundingPhaseFinalized, 'fundingPhase is FundingPhaseFinalized');
            return web3.eth.getBalance(contractOwnerAddress);
        }).catch(e=>{
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('finalization should have failed');
            }
        })
    });


    it('should not allow tokens to be traded since funding goal has not been reached', function() {
        return PeerityToken.deployed().then(net => {
            return net.transfer(accounts[1], 10, {
                from: accounts[0],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('trading did not fail');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('trading did not fail');
            }
        });
    });

    it('should allow refunding', function() {
        let net = null;
        let initialBalance = 0;
        let gasUsed = 0;
        const gasPrice = 20000000000;
        return PeerityToken.deployed().then(instance => {
            net = instance;
            return web3.eth.getBalance(accounts[0]);
        }).then(balance => {
            initialBalance = convertInt(balance);
            return net.refund({
                from: accounts[0],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(ret => {
            assert.ok(getEvent('LogRefund', ret), 'should log a Refund event');
            return lastBlock();
        }).then(lastBlock => {
            gasUsed = lastBlock.gasUsed + 1; // Add some extra gas to work around rounding errors
            return web3.eth.getBalance(accounts[0]);
        }).then(balance => {
            assert.ok(balance >= (initialBalance+standardBid-(gasUsed*gasPrice)), 'balance is not correctly updated');
        }).catch((e) => {
            assert.fail('could not refund');
        });
    });

    it('should not allow to start redeeming period', function() {
        let net = null;
        return PeerityToken.deployed().then(instance => {
            net = instance;
            return net.startRedeeming({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('should not be possible to start redeeming phase');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('should not be possible to start redeeming phase');
            }
        });
    });

    it('should not allow to redeem tokens', function() {
        let net = null;
        return PeerityToken.deployed().then(instance => {
            net = instance;
            return net.redeemTokens("0x3D7D9AF2BF88E91A9D73D10F79C278424DDC89D83D7D9AF2BF88E91A9D73D45A", {
                from: accounts[0],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('should not be possible to redeem');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('should not be possible to redeem');
            }
        });
    });

});
