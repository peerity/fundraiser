var PeerityToken = artifacts.require("PeerityToken");
var assert = require('assert');
var BigNumber = require('bignumber.js');
BigNumber.config({ ERRORS: false });

async function mineBlocks(num=1) {
    for (let i=0; i<num; ++i) {
        await new Promise(function(resolve, reject) { web3.currentProvider.sendAsync({ jsonrpc: "2.0", method: "evm_mine", id: i }, function(err, result) { resolve(); }); });
    }
}

function blockNumber() {
    return new Promise(function(resolve, reject) {
        web3.currentProvider.sendAsync({ jsonrpc: "2.0", method: "eth_blockNumber", id: 0x05 }, function(err, result) { resolve(parseInt(result.result)) });
    });
}

function convertInt(val) {
    return val.toNumber();
}

function snapshot() {
    return new Promise(function(resolve, reject) {
        web3.currentProvider.sendAsync({ jsonrpc: "2.0", method: "evm_snapshot", id: 0xabcd }, function(err, result) { resolve(); });
    });
}

function revert() {
    return new Promise(function(resolve, reject) {
        web3.currentProvider.sendAsync({ jsonrpc: "2.0", method: "evm_revert", id: 0xabcd }, function(err, result) { resolve(); });
    });
}

function reset() {
    return new Promise(function(resolve, reject) {
        web3.currentProvider.sendAsync({ jsonrpc: "2.0", method: "evm_reset", id: 0xabce }, function(err, result) { resolve(); });
    });
}

function getEvent(event, result) {
    for (let i = 0; i < result.logs.length; i++) {
        const log = result.logs[i];

        if (log.event === event) {
            return log;
        }
    }
    return undefined;
}

contract('PeerityToken', function(accounts) {

    let fundingStartBlock = 0;
    let fundingPauseBlock = 0;
    let fundingEndBlock = 0;
    let tierOneExchangeRate = 0;
    let tierTwoExchangeRate = 0;
    let tierThreeExchangeRate = 0;
    let ethReceivedCap = 0;
    let blockDay= 0;
    let ethReceivedMin = 0;
    let tokenCreationCap = 0;
    let tokenMin = 0;
    let contractOwnerAddress = '';
    const initialFundBalance = 999580000200000000000; // there is already some gas deducted for the contract


    let tierOneTokenCap = 0;
    let tierOneTokenFlex = 0;
    let tierTwoTokenCap = 0;
    // match contract enum FundingPhase { One, Paused, Finalized, Redeeming } 
    const enumFundingPhaseOne = 0;
    const enumFundingPhaseTwo = 1;
    const enumFundingPhaseFinalized = 2;
    const enumFundingPhaseRedeeming = 3;

    // match contract enum ContractState { Paused, Emergency } 
    const enumContractStateRunning = 0;
    const enumContractStatePaused = 1;
    const enumContractStateEmergency = 2;

    const weiToEther = 1000000000000000000;
    const standardBid =10000000000000000000; //10 ether
    const epicBid = 400000000000000000000;
    
    before(async function() {
        await reset();
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.fundingStartBlock.call();
        }).then(result => {
            fundingStartBlock = convertInt(result);
            return pt.fundingEndBlock.call();
        }).then(result=>{
            fundingEndBlock = convertInt(result);
            return pt.fundingPauseBlock.call();
        }).then(result => {
            fundingPauseBlock = convertInt(result);
            return pt.ETH_RECEIVED_CAP.call();
        }).then(result => {
            ethReceivedCap = convertInt(result);
            return pt.ETH_RECEIVED_MIN.call();
        }).then(result => {
            ethReceivedMin = convertInt(result);
            return pt.TOKEN_CREATION_CAP.call();
        }).then(result => {
            tokenCreationCap = convertInt(result);
            return pt.TOKEN_MIN.call();
        }).then(result => {
            tokenMin = convertInt(result);
            return pt.contractOwnerAddress.call();
        }).then(result => {
            contractOwnerAddress = result;
            assert(true, "contract owner is "+contractOwnerAddress);
            return pt.BLOCK_DAY.call();
        }).then(result => {
            blockDay = convertInt(result);
            return pt.PHASE_TWO_CREATION_CAP.call();
        }).then(result => {
            tierTwoTokenCap = convertInt(result);
            return pt.PHASE_ONE_CREATION_CAP.call();
        }).then(result=>{
            tierOneTokenCap = convertInt(result);
            return pt.PHASE_ONE_CREATION_FLEX.call();
        }).then(result=>{
            tierOneTokenFlex=convertInt(result);
        });
    });


    /**
     *
     *
     *  PHASE 1 SALE TESTS
     *
     *
     */

    it('should have exactly 92,500,000 tokens, with 18 decimal places',function(){
        return PeerityToken.deployed().then(instance => {
            return instance.TOKEN_CREATION_CAP.call()
        }).then((supply) => {
            let supplyInt = convertInt(supply);
            expectedSupply = new BigNumber(92500000000000000000000000);
            assert.ok(expectedSupply.equals(supplyInt));
            let max = 925 * (10**5) * 10**18;
            assert.ok(expectedSupply.equals(max));
        })
    });

    it('should start at block 4', function() {
        return PeerityToken.deployed().then(instance => {
            return blockNumber();
        }).then(block => {
            assert.equal(block, 4, 'after deploying we should be at block 4, perhaps you should restart testrpc');
        });
    });

    it('Contract should have the initialFundBalance minus used gas', function() {
        const gasUsed = 2099999;
        const gasPrice = 20000000000;

        return PeerityToken.deployed()
            .then(() => {
                return web3.eth.getBalance(contractOwnerAddress);
            }).then(balance => {
                assert.ok(balance >= initialFundBalance - (gasUsed * gasPrice), 'ethFundDeposit wrong initial balance');
            }).catch(() => {
                assert.fail('could not get balance of ethFundDeposit');
            });
    });


    it('should have a fundingStartBlock in the future', function() {
        return blockNumber().then(currentBlock => {
            assert.ok(currentBlock < (fundingStartBlock - 1), 'fundingStartBlock is not in the future');
        });
    });

    it('is not yet finalized/redeeming', function() {
        return PeerityToken.deployed().then(pt => {
            return pt.fundingPhase.call();
        }).then(fundingPhase => {
            assert.ok(convertInt(fundingPhase) === enumFundingPhaseOne, 'should not be finalized');
        })
    });

    it('no finalization before the beginning', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.finalize({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('should not be possible to finalize');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('should not be possible to finalize');
            }
        });
    });

    it('no redeeming period before the beginning', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.startRedeeming({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('should not be possible to start redeeming phase');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('should not be possible to start redeeming phase');
            }
        });
    });

    it('should not issue tokens before the beginning', function() {
        return PeerityToken.deployed().then(pt => {
            return pt.createTokens({
                from: accounts[0],
                value: standardBid,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('token creation did not fail');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('Expected generic Error to be thrown, but threw some other error type -> ' + e.name);
            }
        });
    });

    it('should allow setting exchange tiers during presale', function() {
        //these should be tokens per eth.  Because we have the same number of decimals, it works with wei.  Keep decimals the same.
        let e1 = 1500;
        let e2 = 1000;
        let e3 = 750;
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return blockNumber();
        }).then(() => {
            return pt.setTierExchangeRate(e1, e2, e3, {
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.tierOneExchangeRate.call();
        }).then(tierOneEx => {
            tierOneExchangeRate = tierOneEx;
            assert.equal(e1, tierOneExchangeRate, 'setting exchange tiers failed: expected ' + e1 + ' but was ' + tierOneExchangeRate );
            return pt.tierTwoExchangeRate.call();
        }).then(tierTwoEx => {
            tierTwoExchangeRate = convertInt(tierTwoEx);
            assert.equal(e2, tierTwoExchangeRate, 'setting exchange tiers failed: expected ' + e2 + ' but was ' + tierTwoEx );
            return pt.tierThreeExchangeRate.call();
        }).then(tierThreeEx => {
            tierThreeExchangeRate = convertInt(tierThreeEx);
            assert.equal(e3, tierThreeExchangeRate, 'setting exchange tiers failed: expected ' + e3 + ' but was ' + tierThreeExchangeRate );

        }).catch(() => {
            assert.fail('setting exchange tiers during presale failed');
        });
    });



    it('take a snapshot of the blockchain', function() {
       return snapshot();
    });


    it('should allow giving tokens from presale', function() {
        let pt = null;
        const weis = standardBid;
        let tokenGrant = 20*(10**6) * weiToEther; //20 mil peerity, 18 decimals
        let blocknumber = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.grantPresaleTokens("0x3D7D9AF2BF88E91A9D73D10F79C278424DDC89D83D7D9AF2BF88E91A9D73D45A", tokenGrant, {
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.balanceOf.call("0x3D7D9AF2BF88E91A9D73D10F79C278424DDC89D83D7D9AF2BF88E91A9D73D45A");
        }).then(balance => {
            assert.equal(balance, tokenGrant, 'got wrong amount of tokens');
        }).catch(() => {
            assert.fail('Granting tokens failed');
        });
    });


    it('should not allow giving tokens without being owner', function() {
        let pt = null;
        const weis = standardBid;
        let tokenGrant = 10000;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return blockNumber();
        }).then(() => {
            return pt.grantPresaleTokens("0x3D7D9AF2BF88E91A9D73D10F79C278424DDC89D83D7D9AF2BF88E91A9D73D45A", tokenGrant, {
                from: "0x3D7D9AF2BF88E91A9D73D10F79C278424DDC89D8",
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.balanceOf.call("0x3D7D9AF2BF88E91A9D73D10F79C278424DDC89D83D7D9AF2BF88E91A9D73D45A");
        }).then(balance => {
            assert.fail("Tokens granted by non-owner.")
        }).catch(() => {
            assert.ok(true);
        });
    });


    it('should issue tokens for the correct price in phase 1', function() {
        let pt = null;
        const weis = standardBid;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return blockNumber();
        }).then(currentBlock => {
            // mine blocks to get us to the first block of funding
            return mineBlocks(fundingStartBlock - currentBlock - 1);
        }).then(() => {
            return pt.createTokens({
                from: accounts[0],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.balanceOf.call(accounts[0]);
        }).then(balance => {
            assert.equal(balance, weis * tierOneExchangeRate, 'got wrong amount of tokens');
        }).catch(() => {
            assert.fail('token creation failed');
        });
    });

    it('should NOT allow changing rates after sale begins', function() {
        let e1 = 275;
        let e2 = 325;
        let e3 = 500;
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.setTierExchangeRate(e1, e2, e3, {
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).catch((e) => {
            if (e.name == 'Error') {
                assert.ok(true, "Can't change exchange rates after fundraising starts");
            } else {
                assert.fail('Could change exchange rates after launch');
            }
        })
    });

    it('should not allow people to buy the pot in phase 1', function() {
        let pt = null;
        const weis = epicBid;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.createTokens({
                from: accounts[5],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).catch(() => {
            assert.ok('token creation failed');
        });
    })


    it('should not allow people to buy beyond the phase1 flex limit', function() {
        let pt = null;
        const weis = tierOneTokenFlex / tierOneExchangeRate;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.createTokens({
                from: accounts[7],
                value: weis+ standardBid,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).catch(() => {
            assert.ok('token creation failed');
        });
    });

    it('should not allow people to buy beyond the phase1 individual limit', function() {
        let pt = null;
        const weis = 705*weiToEther;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.createTokens({
                from: accounts[21],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).catch(() => {
            assert.ok('token creation failed');
        });
    })

    it('should allow people to buy up to phase1 limit but below flex', function() {
        let pt = null;
        const weis = 699*weiToEther; //299 ETH, below the buy cap
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.createTokens({
                from: accounts[10],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret)=> {
            assert.ok(getEvent("LogCreatePEER", ret), "Bought peer");
            return pt.totalSupply.call();
        }).then((ret)=> {
            let ans = convertInt(ret)/ (10**18);
            return pt.createTokens({
                from: accounts[12],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret)=>{
            assert.ok(getEvent("LogCreatePEER", ret), "Bought peer");
            return pt.createTokens({
                from: accounts[11],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret)=>{
            assert.ok(getEvent("LogCreatePEER", ret), "Bought peer");
            return pt.createTokens({
                from: accounts[13],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret)=>{
            assert.ok(getEvent("LogCreatePEER", ret), "Bought peer");
            return pt.createTokens({
                from: accounts[14],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret)=>{
            assert.ok(getEvent("LogCreatePEER", ret), "Bought peer");
            return pt.createTokens({
                from: accounts[15],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret)=>{
            assert.ok(getEvent("LogCreatePEER", ret), "Bought peer");
            return pt.createTokens({
                from: accounts[16],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret)=>{
            assert.ok(getEvent("LogCreatePEER", ret), "Bought peer");
            return pt.createTokens({
                from: accounts[17],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret)=>{
            assert.ok(getEvent("LogCreatePEER", ret), "Bought peer");
            return pt.createTokens({
                from: accounts[18],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret)=>{
            assert.ok(getEvent("LogCreatePEER", ret), "Bought peer");
            return pt.createTokens({
                from: accounts[20],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret)=>{
            assert.ok(getEvent("LogCreatePEER", ret), "Bought peer");
            return pt.createTokens({
                from: accounts[19],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret)=>{
            assert.ok(getEvent("LogCreatePEER", ret), "Bought peer");
            return pt.createTokens({
                from: accounts[21],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret)=>{
            assert.ok(getEvent("LogCreatePEER", ret), "Bought peer");
            return pt.createTokens({
                from: accounts[22],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret)=>{
            assert.ok(getEvent("LogCreatePEER", ret), "Bought peer");
            return pt.createTokens({
                from: accounts[23],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret)=>{
            assert.ok(getEvent("LogCreatePEER", ret), "Bought peer");
            return pt.createTokens({
                from: accounts[24],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret)=>{
            assert.ok(getEvent("LogCreatePEER", ret), "Bought peer");
            return pt.createTokens({
                from: accounts[25],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret)=>{
            assert.ok(getEvent("LogCreatePEER", ret), "Bought peer");
            return pt.createTokens({
                from: accounts[26],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret)=>{
            assert.ok(getEvent("LogCreatePEER", ret), "Bought peer");
            return pt.createTokens({
                from: accounts[27],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret)=>{
            assert.ok(getEvent("LogCreatePEER", ret), "Bought peer");
            return pt.createTokens({
                from: accounts[28],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret)=>{
            assert.ok(getEvent("LogCreatePEER", ret), "Bought peer");
            return pt.totalSupply.call();
        }).then((ret)=> {
            let ans = convertInt(ret)/ (10**18);
            return pt.contractState.call();
        }).then(contractState => {
            let paused = convertInt(contractState);
            assert.ok(paused === enumContractStatePaused, 'should be paused after phase 1 cap reached');
        }).then(()=>{
            return pt.fundingPhase.call();
        }).then((fundingPhase)=>{
            assert.ok(convertInt(fundingPhase) === enumFundingPhaseTwo, 'should be in phase 2 after phase 1 cap reached');
        }).catch(e=>{
            assert.fail(e);
        })
    })

    /* END PHASE ONE
    * ======== we are now over Phase One cap and automatically Paused and in Phase Two
    */

    it('should not issue tokens while paused', function() {
        return PeerityToken.deployed().then(pt => {
            return pt.createTokens({
                from: accounts[0],
                value: standardBid,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('token creation did not fail');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('token creation threw something other than expected Error: ' + e.message);
            }
        });
    });

    it('can resume fundraising', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.resumeFundraising({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret) => {
            assert.ok(getEvent('FundraisingResumed', ret), 'should log a resumed event');
            return pt.contractState.call();
        }).then(contractState => {
            assert(contractState, "state is:"+contractState);
            assert.ok(convertInt(contractState) === enumContractStateRunning, 'should resume fundraising');
        })
    });

    it('no finalization before the end when the cap conditions are not met', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return blockNumber();
        }).then(num=>{
            assert(num<fundingEndBlock);
            return pt.finalize({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('should not be possible to finalize');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('should not be possible to finalize');
            }
        });
    });

    it('only the owner can resume', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.resumeFundraising({
                from: accounts[0],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('proceeding should not be allowed');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('proceeding should not be allowed');
            }
        });
    });

    it('should allow us to resume to start Phase 2', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.resumeFundraising({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            })
        }).then(()=>{
            return pt.contractState.call();
        }).then((contractState)=>{
            assert.ok(convertInt(contractState) === enumContractStateRunning, 'should be fundraising'); //TODO: assert equals
        });
    });

    it('should issue tokens for the correct price in Phase 2', function() {
        let pt = null;
        const weis = standardBid;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.createTokens({
                from: accounts[1],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.balanceOf.call(accounts[1]);
        }).then(balance => {
            assert.equal(balance, weis * tierTwoExchangeRate, 'got wrong amount of tokens');
        }).catch(() => {
            assert.fail('token creation did fail');
        });
    });

    it('should issue more tokens for the correct price in phase 2', function() {
        let pt = null;
        const weis = ethReceivedMin;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.createTokens({
                from: accounts[2],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.balanceOf.call(accounts[2]);
        }).then(balance => {
            assert.equal(balance, weis * tierTwoExchangeRate, 'got wrong amount of tokens');
        }).catch(() => {
            assert.fail('token creation did fail');
        });
    });


    it('should not allow tokens to be traded / transferred during bidding in phase 2', function() {
        return PeerityToken.deployed().then(pt => {
            return pt.transfer(accounts[1], 10, {
                from: accounts[0],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('trading did not fail');
        }).catch(e => {
            if (e.name != 'Error') {
                assert.fail('token creation threw something other than expected Error: ' + e.name);
            }
        });
    });

    it('should issue tokens at higher rate up to the limit in phase 2', function() {
        let pt = null;
        let totalsupply = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.getCurrentTokenPrice({
                from: accounts[5],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(price => {
            assert.equal(convertInt(price), tierTwoExchangeRate);
            return pt.totalSupply.call()
        }).then((supply) => {
            totalsupply = convertInt(supply);
            let available = (tierTwoTokenCap - totalsupply);
            let weis = available / tierTwoExchangeRate;
            return pt.createTokens({
                from: accounts[5],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            })
        }).then((ret) => {
            assert.ok(getEvent("LogCreatePEER", ret), "should create some tokens");
            return pt.totalSupply.call();
        }).then(result => {
            let supply = convertInt(result);
            assert.equal(supply, tierTwoTokenCap);
        });
    });

    it('should be in phase two fundraising', function(){
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.fundingPhase.call();
        }).then((fundingPhase)=>{
            assert.ok(convertInt(fundingPhase) === enumFundingPhaseTwo, 'should be fundraising');
        });
    });

    it('should use the third rate after limit in phase 2',function() {
        let pt = null;
        let totalsupply = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.getCurrentTokenPrice({
                from: accounts[5],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(price => {
            assert.equal(convertInt(price), tierThreeExchangeRate);
        })
    });


    it('should issue tokens at third rate now in phase 2',function() {
        let pt = null;
        let totalsupply = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.getCurrentTokenPrice({
                from: accounts[5],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(price => {
            assert.equal(convertInt(price), tierThreeExchangeRate);
            return pt.createTokens({
                from: accounts[7],
                value: standardBid,
                gas: 2099999,
                gasPrice: 20000000000
            });
        })
    });

    it('should not issue tokens that overshoot the eth received cap', function() {
        let net = null;
        const weis = ethReceivedCap; // definitely too much
        return PeerityToken.deployed().then(instance => {
            net = instance;
            return net.createTokens({
                from: accounts[0],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('should not allow token creation overshooting cap');
        }).catch(() => {
            assert.ok(true);
        });
    });

    it('should issue tokens that do NOT overshoot the eth received cap', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            let value = Math.ceil((tokenMin*2) / tierThreeExchangeRate);
            return pt.createTokens({
                from: accounts[1],
                value: value,
                gas: 2599999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.ok(true, "bought a few more tokens");
        }).catch((e) => {
            assert.fail('Was unable to buy tokens: ' + e.message);

        });
    });

    it('should not issue tokens that overshoot the eth received cap', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;

            //obviously way too big since we're bidding the whole cap.
            return net.createTokens({
                from: accounts[0],
                value: ethReceivedCap,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('should not allow token creation overshooting cap');
        }).catch(() => {
            assert.ok(true);
        });
    });

    it('should let us grant tokens up until the end', function() {
        let pt = null;
        let totalsupply = null;
        let hugeGrant  = 20 * (10**6) * 10**18;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.grantPresaleTokens("0x758a0979242d89f2b829b9cc44f77b34384e40d4edbe5f00e94cba71e9999999", hugeGrant, {
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(()=>{
            return pt.balanceOf.call("0x758a0979242d89f2b829b9cc44f77b34384e40d4edbe5f00e94cba71e9999999");
        }).then((balance)=> {
            assert.equal(hugeGrant, convertInt(balance));
        })
    })


    it('should issue tokens at higher rate up to the limit in phase 3', function() {
        let pt = null;
        let totalsupply = null;
        let weis = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.getCurrentTokenPrice({
                from: accounts[7],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(price => {
            assert.equal(convertInt(price), tierThreeExchangeRate);
            return pt.totalSupply.call()
        }).then((supply) => {
            totalsupply = convertInt(supply);
            let available = (tokenCreationCap - totalsupply);
            weis = Math.min(available / tierThreeExchangeRate)-10000000 //rounding error adjust;
            return pt.createTokens({
                from: accounts[7],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            })
        }).then(() => {
            return pt.totalSupply.call()
        }).then(result => {
            let supply = convertInt(result);
            assert.equal(supply, tokenCreationCap)
        }).catch((e)=>{
            assert.fail(e.message);
        });
    });

    it('should not have issued more tokens than available', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.totalSupply.call()
        }).then((supply) => {
            let created = convertInt(supply);
            assert.ok(created = tokenCreationCap, "despite sellout, under token cap")
        }).catch((e) => {
            assert.fail(e.message);
            assert.fail("too many tokens");
        });
    });

    it('should not issue tokens after the funding has ended (because of reaching fundingEndBlock)', function() {
        let pt = null;
        const weis = standardBid;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return blockNumber();
        }).then(currentBlock => {
            // mine necessary amount of blocks
            return mineBlocks(fundingEndBlock - currentBlock); // we need to be > than fundingEndBlock
        }).then(() => {
            return pt.createTokens({
                from: accounts[2],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(balance => {
            assert.fail('token creation did not fail');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('token creation did not fail');
            }
        });
    });

    it('only the owner can call pause', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.pause({
                from: accounts[0],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('pausing should not be allowed');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('Should not be here: '+e.message);
            }
        })
    });



    it('should update balance to greater than 2*standardBid+ethReceivedMin+initialFundBalance-(gasUsed* 2 *gasPrice)) ', function() {
        let pt = null;
        const gasUsed = 34016; // calculated by running the transaction once
        const gasPrice = 20000000000;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            let contractBalance = web3.eth.getBalance(pt.address);
            assert.ok(contractBalance >= ethReceivedMin, "Raised over the minimum");
            assert.ok(contractBalance <= ethReceivedCap, "Raised below maximum");
            return pt.fundingPhase.call();
        }).then((preFinalState)=>{
            assert.ok(convertInt(preFinalState) === enumFundingPhaseTwo);
            return pt.finalize({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return web3.eth.getBalance(contractOwnerAddress);
        }).then(balance => {
            let help = balance.toString();
            assert.ok(balance >= (2*standardBid+ethReceivedMin+initialFundBalance-(gasUsed* 2 *gasPrice)), 'balance is not correctly updated');
        }).catch(() => {

            assert.fail('could not finalize contract');
        });
    });

    it('should allow tokens to be traded after finalization', function() {
        return PeerityToken.deployed().then(pt => {
            return pt.transfer(accounts[3], 10, {
                from: accounts[0],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.ok(true);
        }).catch(() => {
            assert.fail('could not trade tokens');
        });
    });

    it('no finalization after it has been called before', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.finalize({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('should not be possible to finalize');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('Should not be here: '+e.message);
            }
        });
    });


    it('only the owner can start the redeeming period', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.startRedeeming({
                from: accounts[0],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('redeeming should not be allowed');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('Should not be here: '+e.message);
            }
        });
    });

    it('should allow to start the redeeming period', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.startRedeeming({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.fundingPhase.call();
        }).then(fundingPhase => {
            assert.ok(convertInt(fundingPhase) === enumFundingPhaseRedeeming);
        }).catch(() => {
            assert.fail('could not start redeeming period');
        });
    });


    it('should not allow to redeem tokens below the token minimum', function() {
        return PeerityToken.deployed().then(pt => {
            return pt.redeemTokens("0x3D7D9AF2BF88E91A9D73D10F79C278424DDC89D8", {
                from: accounts[3],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('redeeming did not fail');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('redeeming did not fail');
            }
        });
    });


    it('should allow redeeming and burning of tokens', function() {
        let pt = null;
        return PeerityToken.deployed().then(_pt => {
            pt = _pt;
            return pt.redeemTokens("0x3D7D9AF2BF88E91A9D73D10F79C278424DDC89D83D7D9AF2BF88E91A9D73D45A", {
                from: accounts[0],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(ret => {
            assert.ok(getEvent('LogRedeemPEER', ret), 'should log a RedeemPEER event');
            return pt.balanceOf.call(accounts[0]);
        }).then(balance => {
            assert.equal(balance, 0, 'there is still some balance left after redeeming');
        }).catch(() => {
            assert.fail('could not redeem');
        });
    });


    it('should not allow tokens to be traded without having them', function() {
        return PeerityToken.deployed().then(pt => {
            return pt.transfer(accounts[1], 10, {
                from: accounts[6],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(ans=>{
            assert.fail("Trading should fail");
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('trading did not fail');
            }
        });
    });

});
