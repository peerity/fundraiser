var PeerityToken = artifacts.require("PeerityToken");
var assert = require('assert');
var BigNumber = require('bignumber.js');
BigNumber.config({ ERRORS: false });


async function mineBlocks(num=1) {
    for (let i=0; i<num; ++i) {
        await new Promise(function(resolve, reject) { web3.currentProvider.sendAsync({ jsonrpc: "2.0", method: "evm_mine", id: i }, function(err, result) { resolve(); }); });
    }
}

function blockNumber() {
    return new Promise(function(resolve, reject) {
        web3.currentProvider.sendAsync({ jsonrpc: "2.0", method: "eth_blockNumber", id: 0x05 }, function(err, result) { resolve(parseInt(result.result)) });
    });
}

function convertInt(val) {
    return val.toNumber();
}

function snapshot() {
    return new Promise(function(resolve, reject) {
        web3.currentProvider.sendAsync({ jsonrpc: "2.0", method: "evm_snapshot", id: 0xabcd }, function(err, result) { resolve(); });
    });
}

function revert() {
    return new Promise(function(resolve, reject) {
        web3.currentProvider.sendAsync({ jsonrpc: "2.0", method: "evm_revert", id: 0xabcd }, function(err, result) { resolve(); });
    });
}

function reset() {
    return new Promise(function(resolve, reject) {
        web3.currentProvider.sendAsync({ jsonrpc: "2.0", method: "evm_reset", id: 0xabce }, function(err, result) { resolve(); });
    });
}

contract('PeerityToken', function(accounts) {

    let fundingStartBlock = 0;
    let fundingPauseBlock = 0;
    let fundingEndBlock = 0;
    let tierOneExchangeRate = 0;
    let tierTwoExchangeRate = 0;
    let tierThreeExchangeRate = 0;
    let ethReceivedCap = 0;
    let ethReceivedMin = 0;
    let tokenCreationCap = 0;
    let ethFundDeposit = '';
    const initialFundBalance = 999958000020000000000; // there is already some gas deducted for the contract
    const standardBid = 10000000000000000000;
    
    // match contract enum FundingPhase { One, Paused, Finalized, Redeeming } 
    const enumFundingPhaseOne = 0;
    const enumFundingPhaseTwo = 1;
    const enumFundingPhaseFinalized = 2;
    const enumFundingPhaseRedeeming = 3;

    // match contract enum ContractState { Paused, Emergency } 
    const enumContractStateRunning = 0;
    const enumContractStatePaused = 1;
    const enumContractStateEmergency = 2;

    before(async function() {
        await reset();
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.fundingStartBlock.call();
        }).then(result => {
            fundingStartBlock = convertInt(result);
            return pt.fundingPauseBlock.call();
        }).then(result => {
            fundingPauseBlock = convertInt(result);
            return pt.fundingEndBlock.call();
        }).then(result => {
            fundingEndBlock = convertInt(result);
            return pt.ETH_RECEIVED_CAP.call();
        }).then(result => {
            ethReceivedCap = convertInt(result);
            return pt.ETH_RECEIVED_MIN.call();
        }).then(result => {
            ethReceivedMin = convertInt(result);
            return pt.TOKEN_CREATION_CAP.call();
        }).then(result => {
            tokenCreationCap = convertInt(result);
            return pt.contractOwnerAddress.call()
        }).then(result=> {
            ethFundDeposit = result;
        })
    });

    it('should start at block 4', function() {
        return PeerityToken.deployed().then(instance => {
            return blockNumber();
        }).then(block => {
            assert.equal(block, 4, 'after deploying we should be at block 4, perhaps you should restart testrpc');
        });
    });


    it('no finalization before the end when the cap conditions are not met', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.finalize({
                from: ethFundDeposit,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('should not be possible to finalize');
        }).catch(e => {
            if (e.name === 'Error') {
                assert.ok(true);
            } else {
                assert.fail('should not be possible to finalize');
            }
        });
    });

    it('ethFundDeposit should have the initialFundBalance minus used gas', function() {
        const gasUsed = 2099999;
        const gasPrice = 20000000000;

        return PeerityToken.deployed()
            .then(() => {
                return web3.eth.getBalance(ethFundDeposit);
            }).then(balance => {
                assert.ok(balance >= initialFundBalance - (gasUsed * gasPrice), 'ethFundDeposit wrong initial balance');
            }).catch(() => {
                assert.fail('could not get balance of ethFundDeposit');
            });
    });

    it('should allow setting exchange tiers during presale', function() {
        //these should be tokens per eth.  Because we have the same number of decimals, it works with wei.  Keep decimals the same.
        let e1 = 1500;
        let e2 = 1000;
        let e3 = 750;
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.setTierExchangeRate(e1, e2, e3, {
                from: ethFundDeposit,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.tierOneExchangeRate.call();
        }).then(tierOneEx => {
            tierOneExchangeRate = tierOneEx;
            assert.equal(e1, tierOneExchangeRate, 'setting exchange tiers failed: expected ' + e1 + ' but was ' + tierOneExchangeRate );
            return pt.tierTwoExchangeRate.call();
        }).then(tierTwoEx => {
            tierTwoExchangeRate = convertInt(tierTwoEx);
            assert.equal(e2, tierTwoExchangeRate, 'setting exchange tiers failed: expected ' + e2 + ' but was ' + tierTwoEx );
            return pt.tierThreeExchangeRate.call();
        }).then(tierThreeEx => {
            tierThreeExchangeRate = convertInt(tierThreeEx);
            assert.equal(e3, tierThreeExchangeRate, 'setting exchange tiers failed: expected ' + e3 + ' but was ' + tierThreeExchangeRate );

        }).catch((e) => {
            assert.fail('setting exchange tiers during presale failed');
        });
    });


    it('should issue tokens for the correct price in phase 1', function() {
        let pt = null;
        const weis = standardBid;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return blockNumber();
        }).then(currentBlock => {
            // mine blocks to get us to the first block of funding
            return mineBlocks(fundingStartBlock - currentBlock);
        }).then(() => {
            return pt.createTokens({
                from: accounts[0],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.balanceOf.call(accounts[0]);
        }).then(balance => {
            assert.equal(balance, weis * tierOneExchangeRate, 'got wrong amount of tokens');
        }).catch((e) => {
            assert.fail('token creation failed');
        });
    });


    it('should not allow ether retrieval before minimum is raised', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.retrieveEth({
                from: accounts[0],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('should not allow ether retrieval before minimum is raised');
        }).catch(() => {
            assert.ok(true);
        });
    });

    it('should issue tokens for the correct price in phase 1', function() {
        let pt = null;
        const weis = standardBid;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.createTokens({
                from: accounts[12],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.balanceOf.call(accounts[12]);
        }).then(balance => {
            assert.equal(balance, weis * tierOneExchangeRate, 'got wrong amount of tokens');
            //TODO check log for correct tokens
        }).catch(() => {
            assert.fail('token creation failed');
        });
    });


    it('no refunding at this point', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.refund({
                from: accounts[2],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('should not be possible to refund');
        }).catch(e => {
            if (e.name === 'Error') {
                assert.ok(true);
            } else {
                assert.fail('should not be possible to refund');
            }
        });
    });



    it('should sell tokens over the minimum raise for the correct price in phase 1', function() {
        let pt = null;
        const weis = Math.ceil(ethReceivedMin/2);
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.createTokens({
                from: accounts[7],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.balanceOf.call(accounts[7]);
        }).then(balance => {
            assert.equal(balance, weis * tierOneExchangeRate, 'got wrong amount of tokens');
            return pt.createTokens({
                from: accounts[23],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ans)=>{
            return pt.balanceOf.call(accounts[23]);

        }).then(balance=>{
            let bal = convertInt(balance);
            assert.equal(bal, weis * tierOneExchangeRate, 'got wrong amount of tokens');
            return pt.createTokens({
                from: accounts[3],
                value: standardBid,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).catch((e) => {
            assert.fail('token creation failed');
        });
    });

    it('should allow ether retrieval after minimum is raised', function() {
        let pt = null;
        const gasUsed = 209999;
        const gasPrice = 20000000000;
        let fundBase = 0;
        let ethReceived = 0;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return web3.eth.getBalance(ethFundDeposit);
        }).then(bal=>{
            fundBase = convertInt(bal);
            return web3.eth.getBalance(pt.address);
        }).then(bal=>{
            let balance = convertInt(bal);
            assert(balance>= ethReceivedMin);
            ethReceived = balance;
            return pt.retrieveEth({
                from: ethFundDeposit,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return web3.eth.getBalance(ethFundDeposit);
        }).then(balance => {
            let expected = fundBase+ethReceived - gasPrice*gasUsed;
            let final = convertInt(balance)
            assert.ok(final >= expected, 'balance is not correctly updated');
        }).catch((e) => {
            assert.fail('failed to retrieve eth');
        });
    });


    it('should refuse to issue tokens after pause block', function() {
        let pt = null;
        const weis = standardBid;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return blockNumber();
        }).then(currentBlock => {
            // mine necessary amount of blocks
            return mineBlocks(fundingPauseBlock - currentBlock - 1);
        }).then(()=>{
            return pt.createTokens({
                from: accounts[1],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).catch((e)=>{
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('Could buy tokens');
            }
        })
    });

    it('should allow Peerity to end Phase 1', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.endPhaseOne({
                from: ethFundDeposit,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(()=>{
            return pt.fundingPhase.call();
        }).then((fundingPhase)=>{
            assert.ok(convertInt(fundingPhase) === enumFundingPhaseTwo, 'should be paused');
        });
    });

    it('should allow us to resume from Phase 1 to Phase 2', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.resumeFundraising({
                from: ethFundDeposit,
                gas: 2099999,
                gasPrice: 20000000000
            })
        }).then(()=>{
            return pt.contractState.call();
        }).then((contractState)=>{
            assert.ok(convertInt(contractState) === enumContractStateRunning, 'should be fundraising'); //TODO: assert equals
        });
    });

    it('should issue tokens correctly', function() {
        let pt = null;
        const weis =  2 * standardBid;

        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.createTokens({
                from: accounts[16],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.balanceOf.call(accounts[16]);
        }).then(balance => {
            let bal = convertInt(balance);
            let expected = weis * tierTwoExchangeRate;
            assert.equal(bal, expected, 'got wrong amount of tokens');
            return web3.eth.getBalance(pt.address);
        }).then(balance=>{
            let total = convertInt(balance)
            assert(total==weis);
        }).catch((e) => {
            assert.fail('token creation did fail');
        });
    });

    it('no refunding at this point', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.refund({
                from: accounts[0],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('should not be possible to refund');
        }).catch(e => {
            if (e.name === 'Error') {
                assert.ok(true);
            } else {
                assert.fail('should not be possible to refund');
            }
        });
    });

    it('should not allow tokens to be traded before finalization', function() {
        return PeerityToken.deployed().then(pt => {
            return pt.transfer(accounts[1], 10, {
                from: accounts[0],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('trading did not fail');
        }).catch(() => {
            assert.ok(true);
        });
    });

    it('should not allow early finalization', function() {
        let pt = null;
        const gasUsed = 4320053 + 31869; // calculated by running the transaction once
        const gasPrice = 20000000000;

        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.finalize({
                from: ethFundDeposit,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).catch((e) => {
            if (e.name === 'Error') {
                assert.ok(true);
            } else {
                assert.fail('should not be finalizeable yet');
            }
        });
    });

    it('should allow finalization after end offundraiser', function() {
        let pt = null;
        const weis = standardBid;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return blockNumber();
        }).then(currentBlock => {
            // mine blocks to get us to the first block of funding
            return mineBlocks(fundingEndBlock - currentBlock);
        }).then(() => {
            return pt.finalize({
                from: ethFundDeposit,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(()=>{
            return web3.eth.getBalance(ethFundDeposit);
        }).then((bal)=>{
            let total = 4 * standardBid;
            assert(convertInt(bal)>total);
        }).catch((e) => {
            assert.fail('Finalization failed');
        });
    });

    it('should allow tokens to be traded after finalization', function() {
        let pt = null
        let beginBal = 0;
        let onePEER = 1000000000000000000;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.balanceOf.call(accounts[16]);
        }).then((bal)=>{
            beginBal = convertInt(bal);
            //one PEER
            return pt.transfer(accounts[3], onePEER, {
                from: accounts[16],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.ok(true);
            return pt.balanceOf.call(accounts[16]);
        }).then((bal)=>{
            assert.equal(beginBal-onePEER, convertInt(bal));
        }).catch((e) => {
            assert.fail('could not trade tokens');
        });
    });

    it("should fail when trying to transfer too many tokens", function(done) {
        PeerityToken.deployed().then(pt => {
            return pt.transfer.call(accounts[0], 10000000000000000000000, {
                from: accounts[4],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((result)=> {
            assert.equal(false, result);
            done();
        }).catch((e)=>{
            if (e.name == 'Error') {
                assert.ok(true);
                done();
            } else {
                assert.fail('Token transfer did not fail');
                done();
            }
        });
    });


    it("should approve 10 to accounts[4]", function(done) {
        let pt = null;
        PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.approve(accounts[4], 10, {
                from: accounts[3],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(function (result) {
            return pt.allowance.call(accounts[3], accounts[4]);
        }).then(function (result) {
            assert.strictEqual(result.toNumber(), 10);
            done();
        }).catch(done);
    });

    it("transferFrom works", function(done) {
        let pt = null;
        PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.transferFrom(accounts[3], accounts[4], 5, {
                from: accounts[4],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(function (result) {
            return pt.allowance.call(accounts[3], accounts[4]);
        }).then(function (result) {
            assert.strictEqual(result.toNumber(), 5);
            return pt.balanceOf.call(accounts[4]);
        }).then(function (result) {
            assert.strictEqual(result.toNumber(), 5);
            done();
        }).catch(done);
    });

    it("fails for transfer from account with no allowance", function() {
        let pt = null;
        PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.transferFrom.call(accounts[3], accounts[4], 5, {
                from: accounts[2],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(function (result) {
            assert.equal(result, false);
        }).catch(e=>{
            assert.fail(e);
        });
    });

    it("fails for transfer from account with not enough allowance", function() {
        let pt = null;
        PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.transferFrom.call(accounts[3], accounts[4], 6, {
                from: accounts[4],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(function (result) {
              assert.equal(result, false);
        }).catch(e=>{
            assert.fail(e);
        });
    });
});
