// Specifically request an abstraction for PeerityToken
var PeerityToken = artifacts.require("PeerityToken");
var assert = require('assert');
var BigNumber = require('bignumber.js');
BigNumber.config({ ERRORS: false });

async function mineBlocks(num=1) {
    for (let i=0; i<num; ++i) {
        await new Promise(function(resolve, reject) { web3.currentProvider.sendAsync({ jsonrpc: "2.0", method: "evm_mine", id: i }, function(err, result) { resolve(); }); });
    }
}

function blockNumber() {
    return new Promise(function(resolve, reject) {
        web3.currentProvider.sendAsync({ jsonrpc: "2.0", method: "eth_blockNumber", id: 0x05 }, function(err, result) { resolve(parseInt(result.result)) });
    });
}

function convertInt(val) {
    return val.toNumber();
}

function snapshot() {
    return new Promise(function(resolve, reject) {
        web3.currentProvider.sendAsync({ jsonrpc: "2.0", method: "evm_snapshot", id: 0xabcd }, function(err, result) { resolve(); });
    });
}

function revert() {
    return new Promise(function(resolve, reject) {
        web3.currentProvider.sendAsync({ jsonrpc: "2.0", method: "evm_revert", id: 0xabcd }, function(err, result) { resolve(); });
    });
}

function reset() {
    return new Promise(function(resolve, reject) {
        web3.currentProvider.sendAsync({ jsonrpc: "2.0", method: "evm_reset", id: 0xabce }, function(err, result) { resolve(); });
    });
}

function getEvent(event, result) {
    for (let i = 0; i < result.logs.length; i++) {
        const log = result.logs[i];

        if (log.event === event) {
            return log;
        }
    }
    return undefined;
}

contract('PeerityToken', function(accounts) {

    let fundingStartBlock = 0;
    let fundingPauseBlock = 0;
    let fundingEndBlock = 0;
    let tierOneExchangeRate = 0;
    let tierTwoExchangeRate = 0;
    let tierThreeExchangeRate = 0;
    let ethReceivedCap = 0;
    let blockDay= 0;
    let ethReceivedMin = 0;
    let tokenCreationCap = 0;
    let tokenMin = 0;
    let contractOwnerAddress = '';
    const initialFundBalance = 999580000200000000000; // there is already some gas deducted for the contract
    const standardBid = 10000000000000000000;
    const epicBid = 400000000000000000000;

    let tierOneTokenCap = 0;
    let tierTwoTokenCap = 0;
    // match contract enum FundingPhase { One, Paused, Finalized, Redeeming } 
    const enumFundingPhaseOne = 0;
    const enumFundingPhaseTwo = 1;
    const enumFundingPhaseFinalized = 2;
    const enumFundingPhaseRedeeming = 3;

    // match contract enum ContractState { Paused, Emergency } 
    const enumContractStateRunning = 0;
    const enumContractStatePaused = 1;
    const enumContractStateEmergency = 2;
    
    before(async function() {
        await reset();
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.fundingStartBlock.call();
        }).then(result => {
            fundingStartBlock = convertInt(result);
            return pt.fundingEndBlock.call();
        }).then(result=>{
            fundingEndBlock = convertInt(result);
            return pt.fundingPauseBlock.call();
        }).then(result => {
            fundingPauseBlock = convertInt(result);
            return pt.ETH_RECEIVED_CAP.call();
        }).then(result => {
            ethReceivedCap = convertInt(result);
            return pt.ETH_RECEIVED_MIN.call();
        }).then(result => {
            ethReceivedMin = convertInt(result);
            return pt.TOKEN_CREATION_CAP.call();
        }).then(result => {
            tokenCreationCap = convertInt(result);
            return pt.TOKEN_MIN.call();
        }).then(result => {
            tokenMin = convertInt(result);
            return pt.contractOwnerAddress.call();
        }).then(result => {
            contractOwnerAddress = result;
            assert(true, "contract owner is "+contractOwnerAddress);
            return pt.BLOCK_DAY.call();
        }).then(result => {
            blockDay = convertInt(result);
            return pt.PHASE_TWO_CREATION_CAP.call();
        }).then(result => {
            tierTwoTokenCap = convertInt(result);
            return pt.PHASE_ONE_CREATION_CAP.call();
        }).then(result=>{
            tierOneTokenCap = convertInt(result);
        });
    });


    /**
     *
     *
     *  PHASE 1 SALE TESTS
     *
     *
     */

    it('should have exactly 92,500,000 tokens, with 18 decimal places',function(){
        return PeerityToken.deployed().then(instance => {
            return instance.TOKEN_CREATION_CAP.call()
        }).then((supply) => {
            let supplyInt = convertInt(supply);
            expectedSupply = new BigNumber(92500000000000000000000000);
            assert.ok(expectedSupply.equals(supplyInt));
            let max = 925 * (10**5) * 10**18;
            assert.ok(expectedSupply.equals(max));
        })
    });

    it('should start at block 4', function() {
        return PeerityToken.deployed().then(instance => {
            return blockNumber();
        }).then(block => {
            assert.equal(block, 4, 'after deploying we should be at block 4, perhaps you should restart testrpc');
        });
    });

    it('Contract should have the initialFundBalance minus used gas', function() {
        const gasUsed = 2099999;
        const gasPrice = 20000000000;

        return PeerityToken.deployed()
            .then(() => {
                return web3.eth.getBalance(contractOwnerAddress);
            }).then(balance => {
                assert.ok(balance >= initialFundBalance - (gasUsed * gasPrice), 'ethFundDeposit wrong initial balance');
            }).catch(() => {
                assert.fail('could not get balance of ethFundDeposit');
            });
    });


    it('should have a fundingStartBlock in the future', function() {
        return blockNumber().then(currentBlock => {
            assert.ok(currentBlock < (fundingStartBlock - 1), 'fundingStartBlock is not in the future');
        });
    });

    it('is not yet finalized/redeeming', function() {
        return PeerityToken.deployed().then(pt => {
            return pt.fundingPhase.call();
        }).then(fundingPhase => {
            assert.ok(convertInt(fundingPhase) === enumFundingPhaseOne, 'should not be finalized');
        })
    });

    it('no finalization before the beginning', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.finalize({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('should not be possible to finalize');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('should not be possible to finalize');
            }
        });
    });

    it('no redeeming period before the beginning', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.startRedeeming({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('should not be possible to start redeeming phase');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('should not be possible to start redeeming phase');
            }
        });
    });

    it('should not issue tokens before the beginning', function() {
        return PeerityToken.deployed().then(pt => {
            return pt.createTokens({
                from: accounts[0],
                value: standardBid,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('token creation did not fail');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('Expected generic Error to be thrown, but threw some other error type -> ' + e.name);
            }
        });
    });

    it('should allow setting exchange tiers during presale', function() {
        //these should be tokens per eth.  Because we have the same number of decimals, it works with wei.  Keep decimals the same.
        let e1 = 1500;
        let e2 = 1000;
        let e3 = 750;
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return blockNumber();
        }).then(() => {
            return pt.setTierExchangeRate(e1, e2, e3, {
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.tierOneExchangeRate.call();
        }).then(tierOneEx => {
            tierOneExchangeRate = tierOneEx;
            assert.equal(e1, tierOneExchangeRate, 'setting exchange tiers failed: expected ' + e1 + ' but was ' + tierOneExchangeRate );
            return pt.tierTwoExchangeRate.call();
        }).then(tierTwoEx => {
            tierTwoExchangeRate = convertInt(tierTwoEx);
            assert.equal(e2, tierTwoExchangeRate, 'setting exchange tiers failed: expected ' + e2 + ' but was ' + tierTwoEx );
            return pt.tierThreeExchangeRate.call();
        }).then(tierThreeEx => {
            tierThreeExchangeRate = convertInt(tierThreeEx);
            assert.equal(e3, tierThreeExchangeRate, 'setting exchange tiers failed: expected ' + e3 + ' but was ' + tierThreeExchangeRate );

        }).catch(() => {
            assert.fail('setting exchange tiers during presale failed');
        });
    });



    it('take a snapshot of the blockchain', function() {
       return snapshot();
    });


    it('should allow giving tokens from presale', function() {
        let pt = null;
        const weis = standardBid;
        let tokenGrant = 1000000000000000000; //one peerity, 18 decimals
        let blocknumber = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.grantPresaleTokens("0x3D7D9AF2BF88E91A9D73D10F79C278424DDC89D83D7D9AF2BF88E91A9D73D45A", tokenGrant, {
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.balanceOf.call("0x3D7D9AF2BF88E91A9D73D10F79C278424DDC89D83D7D9AF2BF88E91A9D73D45A");
        }).then(balance => {
            assert.equal(balance, tokenGrant, 'got wrong amount of tokens');
        }).catch(() => {
            assert.fail('Granting tokens failed');
        });
    });


    it('should not allow giving tokens without being owner', function() {
        let pt = null;
        const weis = standardBid;
        let tokenGrant = 10000;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return blockNumber();
        }).then(() => {
            return pt.grantPresaleTokens("0x3D7D9AF2BF88E91A9D73D10F79C278424DDC89D83D7D9AF2BF88E91A9D73D45A", tokenGrant, {
                from: "0x3D7D9AF2BF88E91A9D73D10F79C278424DDC89D8",
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.balanceOf.call("0x3D7D9AF2BF88E91A9D73D10F79C278424DDC89D83D7D9AF2BF88E91A9D73D45A");
        }).then(balance => {
            assert.fail("Tokens granted by non-owner.")
        }).catch(() => {
            assert.ok(true);
        });
    });


    it('should issue tokens for the correct price in phase 1', function() {
        let pt = null;
        const weis = standardBid;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return blockNumber();
        }).then(currentBlock => {
            // mine blocks to get us to the first block of funding
            return mineBlocks(fundingStartBlock - currentBlock - 1);
        }).then(() => {
            return pt.createTokens({
                from: accounts[0],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.balanceOf.call(accounts[0]);
        }).then(balance => {
            assert.equal(balance, weis * tierOneExchangeRate, 'got wrong amount of tokens');
            //TODO check log for correct tokens
        }).catch(() => {
            assert.fail('token creation failed');
        });
    });

    /**
     * Manual calc to prove sanity.
     */
    it('should allow the purchase of 3 peerity', function() {
        let pt = null;
        const weis = (3* tokenMin)/tierOneExchangeRate;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.createTokens({
                from: accounts[7],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(()=>{
            return pt.balanceOf(accounts[7], {
                from: accounts[7],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(balance=>{
            assert.equal(convertInt(balance), 3*tokenMin);
            assert.equal(3000000000000000000, convertInt(balance));
        })
    })

    it('should NOT allow changing rates after sale begins', function() {
        let e1 = 275;
        let e2 = 325;
        let e3 = 500;
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.setTierExchangeRate(e1, e2, e3, {
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).catch((e) => {
            if (e.name == 'Error') {
                assert.ok(true, "Can't change exchange rates after fundraising starts");
            } else {
                assert.fail('Could change exchange rates after launch');
            }
        })
    });

    it('should not allow people to buy the pot in phase 1', function() {
        let pt = null;
        const weis = epicBid;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.createTokens({
                from: accounts[5],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).catch(() => {
            assert.ok('token creation failed');
        });
    })
//TODO: is pause fundingPhase or contractState
    it('can pause while in fundraising', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.pause({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((pause) => {
            assert.ok(getEvent('FundraisingPaused', pause), 'should have a pause event');
            return pt.contractState.call();
        }).then(contractState => {
            assert(contractState, "state is "+contractState);
            assert.ok(convertInt(contractState) === enumContractStatePaused, 'should be paused');
        })
    });

    it('should not issue tokens while paused', function() {
        return PeerityToken.deployed().then(pt => {
            return pt.createTokens({
                from: accounts[0],
                value: standardBid,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('token creation did not fail');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('token creation threw something other than expected Error: ' + e.name);
            }
        });
    });

    it('can proceed to fundraising', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.resumeFundraising({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((ret) => {
            assert.ok(getEvent('FundraisingResumed', ret), 'should log a resumed event');
            return pt.fundingPhase.call();
        }).then(fundingPhase => {
            assert(fundingPhase, "state is:"+fundingPhase);
            assert.ok(convertInt(fundingPhase) === enumFundingPhaseOne, 'should resume fundraising');
        })
    });

    it('should not issue tokens below the token minimum in tier one', function() {
        
        var weis = new BigNumber((tokenMin) / tierOneExchangeRate);
        weis = weis.toFixed(0)-20;
        return PeerityToken.deployed().then(pt => {
            return pt.createTokens({
                from: accounts[0],
                value: weis, //maintain 2 decimal digits
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('token creation did not fail');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('token creation threw something other than expected Error: ' + e.message);
            }
        });
    });

    it('should not allow tokens to be traded during bidding in phase 1', function() {
        return PeerityToken.deployed().then(pt => {
            return pt.transfer(accounts[1], 10, {
                from: accounts[0],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('trading did not fail');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('trading did not fail');
            }
        });
    });


    it('no finalization before the end when the cap conditions are not met', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.finalize({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('should not be possible to finalize');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('should not be possible to finalize');
            }
        });
    });


    it('should refuse to issue tokens after pause block', function() {
        let pt = null;
        const weis = standardBid;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return blockNumber();
        }).then(currentBlock => {
            // mine necessary amount of blocks
            return mineBlocks(fundingPauseBlock - currentBlock - 1);
        }).then(()=>{
            return pt.createTokens({
                from: accounts[1],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).catch((e)=>{
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('Could buy tokens');
            }
        })
    });

    it('should allow Peerity to end Phase 1', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return blockNumber();
        }).then(currentBlock => {
            // mine necessary amount of blocks
            return mineBlocks(fundingPauseBlock - currentBlock - 1);
        }).then(()=>{
            return pt.endPhaseOne({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(()=>{
            return pt.fundingPhase.call();
        }).then((fundingPhase)=>{
            assert.ok(convertInt(fundingPhase) === enumFundingPhaseTwo, 'should be paused');
        });
    });

    it('only the owner can resume', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.resumeFundraising({
                from: accounts[0],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('proceeding should not be allowed');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('proceeding should not be allowed');
            }
        });
    });

    it('should allow us to resume from Phase 1 to Phase 2', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.resumeFundraising({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            })
        }).then(()=>{
            return pt.contractState.call();
        }).then((contractState)=>{
            assert.ok(convertInt(contractState) === enumContractStateRunning, 'should be fundraising'); //TODO: assert equals
        });
    });

    /**
     *
     * END PHASE 1
     * TEST PHASE 2
     *
     */

    it('should have a higer rate in phase 2, even if phase 1 cap was not hit', function() {
        let pt = null;
        let totalsupply = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.totalSupply.call()
        }).then(supply=>{
            assert.ok(convertInt(supply) < tierOneTokenCap);
            return pt.getCurrentTokenPrice({
                from: accounts[5],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then((price)=>{
            assert.equal(convertInt(price), tierTwoExchangeRate);
        });
    });

    it('should issue tokens for the correct price in Phase 2', function() {
        let pt = null;
        const weis = standardBid;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.createTokens({
                from: accounts[1],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.balanceOf.call(accounts[1]);
        }).then(balance => {
            assert.equal(balance, weis * tierTwoExchangeRate, 'got wrong amount of tokens');
        }).catch(() => {
            assert.fail('token creation did fail');
        });
    });

    it('should issue more tokens for the correct price in phase 2', function() {
        let pt = null;
        const weis = ethReceivedMin;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.createTokens({
                from: accounts[2],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.balanceOf.call(accounts[2]);
        }).then(balance => {
            assert.equal(balance, weis * tierTwoExchangeRate, 'got wrong amount of tokens');
        }).catch(() => {
            assert.fail('token creation did fail');
        });
    });


    it('should not allow tokens to be traded during bidding in phase 2', function() {
        return PeerityToken.deployed().then(pt => {
            return pt.transfer(accounts[1], 10, {
                from: accounts[0],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('trading did not fail');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('token creation threw something other than expected Error: ' + e.name);
            }
        });
    });


    it('should issue tokens at higher rate up to the limit in phase 2', function() {
        let pt = null;
        let totalsupply = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.getCurrentTokenPrice({
                from: accounts[5],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(price => {
            assert.equal(convertInt(price), tierTwoExchangeRate);
            return pt.totalSupply.call()
        }).then((supply) => {
            totalsupply = convertInt(supply);
            let available = (tierTwoTokenCap - totalsupply);
            let weis = available / tierTwoExchangeRate;
            return pt.createTokens({
                from: accounts[5],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            })
        }).then((ret) => {
            assert.ok(getEvent("LogCreatePEER", ret), "Hit soft cap");
            assert.ok(getEvent("FundraisingCapTriggered", ret), "Hit soft cap");
            return pt.totalSupply.call()
        }).then(result => {
            let supply = convertInt(result);
            assert.equal(supply, tierTwoTokenCap)

        });
    });

    it('should be in phase two fundraising', function(){
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.fundingPhase.call();
        }).then((fundingPhase)=>{
            assert.ok(convertInt(fundingPhase) === enumFundingPhaseTwo, 'should be fundraising');
        });
    });

    it('should use the third rate after limit in phase 2',function() {
        let pt = null;
        let totalsupply = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.getCurrentTokenPrice({
                from: accounts[5],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(price => {
            assert.equal(convertInt(price), tierThreeExchangeRate);
        })
    });


    it('should issue tokens at third rate now in phase 2',function() {
        let pt = null;
        let totalsupply = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.getCurrentTokenPrice({
                from: accounts[5],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(price => {
            assert.equal(convertInt(price), tierThreeExchangeRate);
            return pt.createTokens({
                from: accounts[7],
                value: standardBid,
                gas: 2099999,
                gasPrice: 20000000000
            });
        })
    });

    it('should not issue tokens that overshoot the eth received cap', function() {
        let net = null;
        const weis = ethReceivedCap; // definitely too much
        return PeerityToken.deployed().then(instance => {
            net = instance;
            return net.createTokens({
                from: accounts[0],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('should not allow token creation overshooting cap');
        }).catch(() => {
            assert.ok(true);
        });
    });

    it('should issue tokens that do NOT overshoot the eth received cap', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            let value = Math.ceil((tokenMin*2) / tierThreeExchangeRate);
            return pt.createTokens({
                from: accounts[1],
                value: value,
                gas: 2599999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.ok(true, "bought a few more tokens");
        }).catch((e) => {
            assert.fail('Was unable to buy tokens: ' + e.message);

        });
    });

    it('should not issue tokens that overshoot the eth received cap', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;

            //obviously way too big since we're bidding the whole cap.
            return net.createTokens({
                from: accounts[0],
                value: ethReceivedCap,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('should not allow token creation overshooting cap');
        }).catch(() => {
            assert.ok(true);
        });
    });

    it('should let us grant tokens up until the end', function() {
        let pt = null;
        let totalsupply = null;
        let hugeGrant  = 20 * (10**6) * 10**18;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.grantPresaleTokens("0x758a0979242d89f2b829b9cc44f77b34384e40d4edbe5f00e94cba71e9999999", hugeGrant, {
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(()=>{
            return pt.balanceOf.call("0x758a0979242d89f2b829b9cc44f77b34384e40d4edbe5f00e94cba71e9999999");
        }).then((balance)=> {
            assert.equal(hugeGrant, convertInt(balance));
        })
    })

    it('should issue tokens at higher rate up to the limit in phase 3', function() {
        let pt = null;
        let totalsupply = null;
        let weis = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.getCurrentTokenPrice({
                from: accounts[7],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(price => {
            assert.equal(convertInt(price), tierThreeExchangeRate);
            return pt.totalSupply.call()
        }).then((supply) => {
            totalsupply = convertInt(supply);
            let available = (tokenCreationCap - totalsupply);
            weis = Math.min(available / tierThreeExchangeRate)-10000000 //rounding error adjust;
            return pt.createTokens({
                from: accounts[7],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            })
        }).then(() => {
            return pt.totalSupply.call()
        }).then(result => {
            let supply = convertInt(result);
            assert.equal(supply, tokenCreationCap)
        }).catch((e)=>{
            assert.fail(e.message);
        });
    });

    it('should not have issued more tokens than available', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.totalSupply.call()
        }).then((supply) => {
            let created = convertInt(supply);
            assert.ok(created = tokenCreationCap, "despite sellout, under token cap")
        }).catch((e) => {
            assert.fail(e.message);
            assert.fail("too many tokens");
        });
    });

    it('should not issue tokens after the funding has ended (because of reaching fundingEndBlock)', function() {
        let pt = null;
        const weis = standardBid;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return blockNumber();
        }).then(currentBlock => {
            // mine necessary amount of blocks
            return mineBlocks(fundingEndBlock - currentBlock); // we need to be > than fundingEndBlock
        }).then(() => {
            return pt.createTokens({
                from: accounts[2],
                value: weis,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(balance => {
            assert.fail('token creation did not fail');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('token creation threw something other than expected Error: ' + e.name);
            }
        });
    });

    it('should allow tokens to be traded after endblock and ether has been raised, no matter what.', function() {
        let pt = null;
        let blocknumber = null;
        let totalEth = null;
        return PeerityToken.deployed().then(instance=> {
            pt=instance;
            return blockNumber()
        }).then((num) => {
            blocknumber = num
            return pt.totalReceivedEth.call();
        }).then(total=>{
            totalEth = convertInt(total);
            if(blocknumber >= fundingEndBlock && total> ethReceivedMin) {
                return pt.transfer(accounts[1], 10, {
                    from: accounts[0],
                    gas: 2099999,
                    gasPrice: 20000000000
                });
            } else {
               assert.fail("should not fail");
            }
        }).then((ans)=>{
            assert.ok("Trades work after min raise and fundingEndBlock happens");
        }).catch(e=>{
            assert.fail("Trades should be allowed")
        })
    });

    it('only the owner can call pause', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.pause({
                from: accounts[0],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('pausing should not be allowed');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('Should not be here: '+e.message);
            }
        })
    });



    it('should allow finalization during phase two', function() {
        let pt = null;
        const gasUsed = 34016; // calculated by running the transaction once
        const gasPrice = 20000000000;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            let contractBalance = web3.eth.getBalance(pt.address);
            assert.ok(contractBalance >= ethReceivedMin, "Raised over the minimum");
            assert.ok(contractBalance <= ethReceivedCap, "Raised below maximum");
            return pt.fundingPhase.call();
        }).then((preFinalState)=>{
            assert.ok(convertInt(preFinalState) === enumFundingPhaseTwo);
            return pt.finalize({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.fundingPhase.call();
        }).then(fundingPhase => {
            let currentstate = convertInt(fundingPhase);
            assert.equal(currentstate, enumFundingPhaseFinalized, 'expected currentstate === enumFundingPhaseFinalized');
            return web3.eth.getBalance(contractOwnerAddress);
        }).then(balance => {
            let help = balance.toString();
            assert.ok(balance >= (2*standardBid+ethReceivedMin+initialFundBalance-(gasUsed* 2 *gasPrice)), 'balance is not correctly updated');
        });
    });

    it('can not pause while finalized', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.pause({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.fundingPhase.call();
        }).then(fundingPhase => {
            assert.fail(convertInt(fundingPhase) === enumFundingPhaseTwo, 'should not be paused');
        }).catch((e)=>{
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('Should not be here: '+e.message);
            }
        })
    });



    it('should allow tokens to be traded after finalization', function() {
        return PeerityToken.deployed().then(pt => {
            return pt.transfer(accounts[3], 10, {
                from: accounts[0],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.ok(true);
        }).catch(() => {
            assert.fail('could not trade tokens');
        });
    });

    it('no finalization after it has been called before', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.finalize({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('should not be possible to finalize');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('Should not be here: '+e.message);
            }
        });
    });


    it('only the owner can start the redeeming period', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.startRedeeming({
                from: accounts[0],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('redeeming should not be allowed');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('Should not be here: '+e.message);
            }
        });
    });

    it('should allow to start the redeeming period', function() {
        let pt = null;
        return PeerityToken.deployed().then(instance => {
            pt = instance;
            return pt.startRedeeming({
                from: contractOwnerAddress,
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            return pt.fundingPhase.call();
        }).then(fundingPhase => {
            assert.ok(convertInt(fundingPhase) === enumFundingPhaseRedeeming);
        }).catch(() => {
            assert.fail('could not start redeeming period');
        });
    });


    it('should not allow to redeem tokens below the token minimum', function() {
        return PeerityToken.deployed().then(pt => {
            return pt.redeemTokens("0x3D7D9AF2BF88E91A9D73D10F79C278424DDC89D8", {
                from: accounts[3],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(() => {
            assert.fail('redeeming did not fail');
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('redeeming did not fail');
            }
        });
    });


    it('should allow redeeming and burning of tokens', function() {
        let pt = null;
        return PeerityToken.deployed().then(_pt => {
            pt = _pt;
            return pt.redeemTokens("0x3D7D9AF2BF88E91A9D73D10F79C278424DDC89D83D7D9AF2BF88E91A9D73D45A", {
                from: accounts[0],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(ret => {
            assert.ok(getEvent('LogRedeemPEER', ret), 'should log a RedeemPEER event');
            return pt.balanceOf.call(accounts[0]);
        }).then(balance => {
            assert.equal(balance, 0, 'there is still some balance left after redeeming');
        }).catch(() => {
            assert.fail('could not redeem');
        });
    });


    it('should not allow tokens to be traded without having them', function() {
        return PeerityToken.deployed().then(pt => {
            return pt.balanceOf(accounts[6], {
                from: accounts[17],
                gas: 2099999,
                gasPrice: 20000000000
            })
        }).then((ans) => {
            let answer = convertInt(ans);
            return pt.transfer(accounts[17], 10, {
                from: accounts[6],
                gas: 2099999,
                gasPrice: 20000000000
            });
        }).then(ans=>{
            assert.fail("Trading should fail");
        }).catch(e => {
            if (e.name == 'Error') {
                assert.ok(true);
            } else {
                assert.fail('trading did not fail');
            }
        });
    });

});
